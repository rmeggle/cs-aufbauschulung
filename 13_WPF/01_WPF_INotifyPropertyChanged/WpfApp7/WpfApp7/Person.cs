﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace WpfApp7
{
    public class Person : INotifyPropertyChanged
    {
        private String _Vorname { get; set; }
        private String _Nachname { get; set; }
        private String _Name { get; set; }

        public String Vorname
        {
            get { return _Vorname; } 
            set {
                _Vorname = value;
                OnPropertyChanged("Vorname");
                OnPropertyChanged("Name");
            }
        }

        public String Nachname
        {
            get { return _Nachname; }
            set {
                _Nachname = value;
                OnPropertyChanged("Nachname");
                OnPropertyChanged("Name");
            }
        }

        public String Name
        {
            get {
                return String.Concat(_Vorname, " ", _Nachname);
            }
            set {
                _Name = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(property));
            }
        }

    }
}
