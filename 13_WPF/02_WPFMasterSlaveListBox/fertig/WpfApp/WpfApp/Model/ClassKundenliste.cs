﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Model
{
    class ClassKundenliste
    {
        private ObservableCollection<ClassKunde> kundenliste;

        public ClassKundenliste()
        {
            Random rnd = new Random();
            int irgendeinTag = rnd.Next(1, 28);
            int irgendeinMonat = rnd.Next(1, 12);
            //int irgendeinTag = 1;
            //int irgendeinMonat = 1;

            kundenliste = new ObservableCollection<ClassKunde>();

            // -------------------------

            ClassKunde Kunde = new ClassKunde() { Vorname = "Donald", Nachname = "Duck" };
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "ELDO Fotoapparat"   , Bestellnummer = "0001", Bestelldatum = new DateTime(2017,irgendeinMonat, irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Digitalradio"       , Bestellnummer = "0002", Bestelldatum = new DateTime(2017,irgendeinMonat, irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Strinlampe"         , Bestellnummer = "0003", Bestelldatum = new DateTime(2017,irgendeinMonat, irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Mokassin-Schuhe"    , Bestellnummer = "0004", Bestelldatum = new DateTime(2017,irgendeinMonat, irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Hantelscheibe"      , Bestellnummer = "0005", Bestelldatum = new DateTime(2017,irgendeinMonat, irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);

            kundenliste.Add(Kunde);

            // -------------------------

            Kunde = new ClassKunde() { Vorname = "Dasy", Nachname = "Duck" };
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Hornhautentferner"  , Bestellnummer = "0006", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Zahnfärbungsradierer", Bestellnummer = "0007", Bestelldatum = new DateTime(2017,irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Haaröl"             , Bestellnummer = "0008", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Glätteisen"         , Bestellnummer = "0009", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Massagekissen"      , Bestellnummer = "0010", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);

            kundenliste.Add(Kunde);

            // -------------------------

            Kunde = new ClassKunde() { Vorname = "Daniel", Nachname = "Düsentrieb" };
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "USB Stick"          , Bestellnummer = "0011", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Powerbank"          , Bestellnummer = "0012", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "MICRO USB Stecker"  , Bestellnummer = "0013", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Steckdosenleiste"   , Bestellnummer = "0014", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);
            Kunde.Bestellung.Add(new ClassBestellung() { Artikel = "Listerklemmen"      , Bestellnummer = "0015", Bestelldatum = new DateTime(2017, irgendeinMonat,irgendeinTag) }); irgendeinTag = rnd.Next(1, 28); irgendeinMonat = rnd.Next(1, 12);

            kundenliste.Add(Kunde);
        }

        public ObservableCollection<ClassKunde> KundenlisteDerKlasse
        {
            get { return kundenliste; }
        }
    }
}
