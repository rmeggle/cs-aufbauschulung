﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppDataGrid
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            // Anstelle mit dem SqlDataAdapter oder OleDbDataAdapter kann auch eine Tabelle
            // direkt zur Laufzeit erstellt werden. 
            // msdn: https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/populating-a-dataset-from-a-dataadapter

            // Erstellen einer Tabelle ohne "Datenbank"
            DataTable tabelle = new DataTable("Namen");
            // ..oder alternativ "namneslos":
            // DataTable tabelle = new DataTable();

            // Wenn es mehrere Tabellen sind, die voneinander 
            // abhängige Daten (relational) beinhalten, dann 
            // muss zuvor ein dataset erstellt werden, welchem
            // man die Tabellen zur Laufzeit zuordnen kann: 
            // Beispiel: 
            //DataSet dataset = new DataSet("MeineDB");
            //DataTable tabelle = dataset.Tables.Add("Namen");


            tabelle.Columns.Add("Vorname", typeof(string));
            tabelle.Columns.Add("Nachname", typeof(string));
            tabelle.Columns.Add("Alter", typeof(int));

            tabelle.Rows.Add("Donald", "Duck", 35);
            tabelle.Rows.Add("Dagobert", "Duck", 77);
            tabelle.Rows.Add("Dasy", "Duck", 26);

            ////// Filtern der Tabelle ist somit möglich: 
            String Filter = "Alter < 40";

            DataRow[] result = tabelle.Select(Filter);
            foreach (DataRow TabellenZeile in result)
            {
                MessageBox.Show(Convert.ToString(TabellenZeile[0]));
            }

            MeinDataGrid.ItemsSource = tabelle.DefaultView;

        }

        private void MeinDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (MeinDataGrid.SelectedValue == null)
                return; // return if there's no row selected

            int ZeilenNr = MeinDataGrid.SelectedIndex + 1;
            object item = MeinDataGrid.SelectedItem;

            String Vorname = ((DataRowView)MeinDataGrid.SelectedItem).Row["Vorname"].ToString();
            String Nachname = ((DataRowView)MeinDataGrid.SelectedItem).Row["Nachname"].ToString();
            String Alter = ((DataRowView)MeinDataGrid.SelectedItem).Row["Alter"].ToString();

            TextBoxVornamename.Text = Vorname;
            TextBoxNachname.Text = Nachname;
            TextBoxAlter.Text = Alter;
        }

    }


}
