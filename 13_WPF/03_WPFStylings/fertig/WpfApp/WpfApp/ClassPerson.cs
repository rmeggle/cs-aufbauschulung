﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp
{
    class ClassPerson
    {
        public string Vorname{ get; set; }
        public string Nachname { get; set; }

        public ClassPerson(String Vorname, String Nachname)
        {
            this.Vorname = Vorname;
            this.Nachname = Nachname; 
        }
    }


}
