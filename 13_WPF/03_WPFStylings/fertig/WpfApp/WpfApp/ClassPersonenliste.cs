﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace WpfApp
{
    class ClassPersonenliste : ObservableCollection<ClassPerson>
    {

        public ClassPersonenliste()
        {
            Add(new ClassPerson("Donald", "Duck" ));
            Add(new ClassPerson("Dasy", "Duck" ));
            Add(new ClassPerson("Dagobert", "Duck" ));
            Add(new ClassPerson("Daniel", "Düsentrieb"));
        }
    }
}
