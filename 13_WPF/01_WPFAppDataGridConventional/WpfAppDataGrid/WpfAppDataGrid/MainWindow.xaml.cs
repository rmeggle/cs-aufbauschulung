﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppDataGrid
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const String connString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CSharpSchulung;Data Source=MEROLAB03\SQLEXPRESS";

        public MainWindow()
        {
            InitializeComponent();


        }

        private void MeinDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid gd = (DataGrid)sender;
            DataRowView row_selected = gd.SelectedItem as DataRowView;
            labelIndex.Content = "Index: " + row_selected[0].ToString();
            textBoxVorname.Text = row_selected[1].ToString();
            textBoxNachname.Text = row_selected[2].ToString();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            String cmdString = "SELECT * FROM [tbl_DatentypBeispiel]";
            using (SqlConnection con = new SqlConnection(connString))
            {
                SqlCommand cmd = new SqlCommand(cmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable("MeineLokaleDatentypBeispiel");
                sda.Fill(dt);

                MeinDataGrid.ItemsSource = dt.DefaultView;

            }
        }
    }
}
