﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp.Model
{
    public class ClassPerson 
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }
    }
}
