﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WpfApp.Model;

namespace WpfApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public List<ClassPerson> Personenliste { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Personenliste = new List<ClassPerson>
            {
                new ClassPerson { Vorname = "Donald", Nachname="Duck" },
                new ClassPerson { Vorname = "Daniel" , Nachname="Düsentrieb" }
            };
            DataContext = this;
        }
    }
}
