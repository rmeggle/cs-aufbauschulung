﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataTable MeineStudenten = new DataTable("DT");

        public MainWindow()
        {
            InitializeComponent();

            MeineStudenten.Columns.Add("Vorname");
            MeineStudenten.Columns.Add("Nachname");
            MeineStudenten.Columns.Add("Ergebnis");

            MeineStudenten.Rows.Add("Donald", "Duck", "85");
            MeineStudenten.Rows.Add("Dasy", "Duck", "32");
            MeineStudenten.Rows.Add("Dagobert", "Duck", "40");
            MeineStudenten.Rows.Add("Daniel", "Düsentrieb", "30");

            GidMeineStudenten.ItemsSource = MeineStudenten.AsDataView();

        }


    }
}
