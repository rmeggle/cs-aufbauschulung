﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace WpfApp
{
    class ClassGetErgebnis : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                int Note = Int32.Parse((string)value);

                if (Note > 40)
                {
                    return "Bestanden";
                }
                else
                {
                    return "Durchgefallen";
                }
            }
            catch (Exception ex )
            {

                return ex.Message;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new object();
        }
    }
}
