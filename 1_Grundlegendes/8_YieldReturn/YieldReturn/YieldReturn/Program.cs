﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YieldReturn
{
    

    class Program
    {
        static IEnumerable<int> MeinZufallszahlen(int AnzahlZufallszahlen)
        {
            Random rand = new Random();
            for (int i = 0; i<=AnzahlZufallszahlen; i++)
            {
                yield return rand.Next(-2, 2);
            }
            
        }

        static void Main(string[] args)
        {

            int[] Zufallszahlen = MeinZufallszahlen(10).ToArray();

            //var MeinePositivenZahlen =
            //    from i in Zufallszahlen
            //    orderby i ascending
            //    where i >= 0
            //    select i;

            //foreach ( int z in MeinePositivenZahlen)
            //{
            //    Console.WriteLine(z);
            //}

            
            foreach ( int z in Zufallszahlen)
            {
                Console.WriteLine(z);
            }

            Console.WriteLine("---");

            var ZufallsZahlenGruppiert =
                Zufallszahlen
                    .GroupBy(z => z)
                    .OrderBy(g => g.Key)
                    .Select(g => new { Zahl = g.Key, Anzahl = g.Count() });

            foreach ( var meineZahl in ZufallsZahlenGruppiert)
            {
                Console.WriteLine($" {meineZahl.Zahl} Anzahl: {meineZahl.Anzahl}");
            }

            Console.ReadLine();
        }
    }
}
