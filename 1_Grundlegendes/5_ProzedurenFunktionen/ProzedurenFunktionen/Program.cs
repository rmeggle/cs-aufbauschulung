﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProzedurenFunktionen
{
    class Program
    {
        static void Main(string[] args)
        {
            int z1 = 47;
            int z2 = 11;

            // wenn eine Methode als static definiert wurde, so kann sie 
            // unmittelbar verwendet werden
            if (istgerade_static(z1)==true)
                Console.WriteLine("{0} ist eine Ganzzahl", z1);
            else
                Console.WriteLine("{0} ist keine Ganzzahl", z1);

            // wenn eine Methode nicht als static definiert wurde, muss sie 
            // instanziert werden
            Program myProgramm = new Program();
            if (myProgramm.istgerade(z2) == true)
                Console.WriteLine("{0} ist eine Ganzzahl", z2);
            else
                Console.WriteLine("{0} ist keine Ganzzahl", z2);

            z1 = 47; z2 = 11;
            tausche(z1, z2);

            z1 = 47; z2 = 11;
            tausche_mit_referenz(ref z1, ref z2);

            int oz1, oz2;
            setze_mit_out(out oz1, out oz2);

            z1 = 47; z2 = 11;
            int oz3, oz4;
            setze_mit_out(z1, z2, out oz3, out oz4);


            Console.ReadLine();

        }

        private static Boolean istgerade_static(int ganzzahl)
        {
            return ((ganzzahl % 2) == 1) ? false : true;
        }

        private Boolean istgerade(int ganzzahl)
        {
            return ((ganzzahl % 2) == 1) ? false : true;
        }

        /// <summary>
        /// CallByValue => in a und b liegt eine Kopie von z1 und z2
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static void tausche(int a, int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
        }

        /// <summary>
        /// "ref" => Referenz auf die Adresse von a und b
        /// Bei ref kann die funktion/prozedur den Wert an diese Adresse verändern
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static void tausche_mit_referenz (ref int a, ref int b )
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
        }

        /// <summary>
        /// "out" => Mittels diesen Schlüsselwortes wird ein Wert innerhalb 
        /// einer Methode gesetzt - es kann aber kein Wert mit out in eine 
        /// Methode übergeben werden. 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public static void setze_mit_out(out int x, out int y)
        {
            x = 47;
            y = 11;
        }

        /// <summary>
        /// Mischen von Parameterdefinitionen ist erlaubt
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public static void setze_mit_out(int a, int b, out int x, out int y)
        {
            x = a;
            y = b;
        }

    }
}
