﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamReaderWriter
{
    class Program
    {
        public static String Textfile;

        static void Main(string[] args)
        {
            //  .--------------.     .--------------.
            //  | SteamWriter  |     | SteamReader  |
            //  | BinaryWriter |     | BinaryReader |
            //  '--------------'     '--------------'
            //          |                    |
            //          |                    |
            //          |                    |
            //          |   .------------.   |
            //          '-->| FileStream |<--'
            //              '------------'
            //                     |
            //                     |
            //                     v
            //                 .-------.
            //                 | Datei |
            //                 '-------'

            // Datenströme (FileStreams) beschreiben jede Art von lesen/schreiben (Datei, Netzwerk, Tastaur...)
            // Den aktuellen Ordner ermitteln (In unserem Fall sind wir in: <PROJEKTORDNER>\bin\Debug: 
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();


            #region "Prüfen, ob eine Datei existiert"

            Textfile = @"\beispieltexte\copy.txt";

            Boolean Existiert;
            String DateiName;
            DateiName = AktuellerOrdner + Textfile;
            Existiert = System.IO.File.Exists(DateiName); // => False
            DateiName = AktuellerOrdner + @"\..\.." + Textfile; // Das Verzeichnis de-referenzieren (2 x "zurück")
            Existiert = System.IO.File.Exists(DateiName); // => True

            #endregion

            // ---------------------------------------------------------------

            #region "Kopier, Verschiebe und Löschvorgänge bei Dateien"

            // Eine Datei kopieren
            String src = AktuellerOrdner + @"\..\..\beispieltexte\src.txt";
            String dest = AktuellerOrdner + @"\..\..\beispieltexte\copy.txt";


            System.IO.File.Copy(src, dest); // ok - Fehler aber, wenn schon vorhanden ist!
            System.IO.File.Copy(src, dest, true); // ok - kein Fehler, wenn Datei schon vorhanden ist!

            // Eine Datei verschieben oder umbenenen: 
            System.IO.File.Move(AktuellerOrdner + @"\..\..\beispieltexte\copy.txt", AktuellerOrdner + @"\..\..\beispieltexte\move.txt");
            // Eine Datei löschen: 
            System.IO.File.Delete(AktuellerOrdner + @"\..\..\beispieltexte\move.txt");

            #endregion

            // ---------------------------------------------------------------

            #region "Prüfen, ob ein Verzeichnis existiert"

            Existiert = System.IO.Directory.Exists(AktuellerOrdner);

            // Spezielle Ordner (="special folders") sind die ab Windows 7 bezeichneten "Bibliotheken" 
            string specialFolder = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
            Existiert = System.IO.Directory.Exists(AktuellerOrdner);

            // Alle Dateien in einem Ordner ermitteln:
            string[] AlleDateien = Directory.GetFiles(AktuellerOrdner);
            string[] AlleVerzeichnisse = Directory.GetDirectories(@"C:\");

            #endregion

            // ---------------------------------------------------------------

            #region "Kopier, Verschiebe und Löschvorgänge bei Verzeichnissen"

            Directory.CreateDirectory(AktuellerOrdner + @"\..\..\beispieltexte\TEST");
            Directory.Delete(AktuellerOrdner + @"\..\..\beispieltexte\TEST");

            #endregion

            // ---------------------------------------------------------------

            #region "Fallbeispiele"

            // Eine sehr große Textdatei: 
            Textfile = @"\beispieltexte\t8.shakespeare.txt";

            //ReadAllText();
            //ReadLineByLine();
            //FileStreamWriter();
            ReadFromWeb();
            #endregion

        }

        static void ReadAllText()
        {
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();
            string text = System.IO.File.ReadAllText(AktuellerOrdner + @"\..\.." + Textfile);

            // Ausgeben des gesamten Textes: 
            System.Console.WriteLine("Contents of {0} = {1}", Textfile, text);

        }

        static void ReadLineByLine()
        {
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();
            String DateiName = AktuellerOrdner + @"\..\.." + Textfile;

            using (var rd = new StreamReader(DateiName))
            {
                while (!rd.EndOfStream)
                {
                    String Zeile;
                    Zeile = rd.ReadLine();
                    System.Console.WriteLine(Zeile);
                }
            }
        }

        static void FileStreamWriter()
        {
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();
            String DateiName = AktuellerOrdner + @"\..\..\beispieltexte\test.txt";

            using (StreamWriter writeToFile = new StreamWriter(DateiName, true))
            { // das "true" beim StreamWriter steht für "append"

                writeToFile.WriteLine("Hallo Welt");

            }
        }

        static void ReadFromWeb()
        {
            String WebSource = @"https://ocw.mit.edu/ans7870/6/6.006/s08/lecturenotes/files/t8.shakespeare.txt";
            System.Net.WebClient client = new System.Net.WebClient();
            Stream stream = client.OpenRead(WebSource);
            StreamReader reader = new StreamReader(stream);
            String content = reader.ReadToEnd();
        }
    }
}
