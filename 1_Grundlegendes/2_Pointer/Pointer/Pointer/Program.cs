﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pointer
{
    class Program
    {

        static void tausche(ref int a, ref int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
        }

        /// <summary>
        /// Projekt -> Eigenschaften -> Build=Unsicheren Code zulassen
        /// </summary>
        /// <param name="args"></param>
        static unsafe void Main(string[] args)
        {
            int x = 10;
            int y = 20;
            int tmp;
            //tausche(ref x, ref y);

            int* PtrAddr_X;
            int* PtrAddr_Y;
            int* PtrAddr_tmp;

            PtrAddr_X = &x;
            PtrAddr_Y = &y;
            PtrAddr_tmp = &tmp;

            *PtrAddr_tmp = *PtrAddr_X;
            *PtrAddr_X = *PtrAddr_Y;
            *PtrAddr_Y = *PtrAddr_tmp;

        }
    }
}
