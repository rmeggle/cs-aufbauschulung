﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// mehr Informationen hier: https://docs.microsoft.com/de-de/dotnet/standard/collections/
/// </summary>
namespace Listen
{
    class Program
    {
        static void Main(string[] args)
        {

            ArrayList MeineListe = new ArrayList();
            MeineListe.Add(1);
            MeineListe.Add("Zeichenkette");
            MeineListe.Add(DateTime.Now);
            MeineListe.Add(true);
            foreach (var item in MeineListe)
            {
                Console.WriteLine(item);
            }

            #region "List"
            // Einsatzgebiet:
            // Überall, wo keine spezialisierte Liste/ Collection passt.
            // Dies ist die Standardliste und somit die wohl am häufigsten verwendete.Mit deren Methoden sollte man sich 
            // genauestens auseinandersetzen, da man häufig damit in Kontakt kommt.
            // 
            // Vorteile:
            // Viele Methoden zum Suchen, Einfügen, Sortieren und Manipulieren der Daten verfügbar.
            // 
            // Nachteile:
            // Einfügen und Entfernen von Elementen am Anfang oder der Mitte der Liste sind relativ teuere Operationen.
            // Größeren Listen landen auf dem LOH(Large Object Heap) => werden Langsam!
            List<int> NummernListe = new List<int>();
            NummernListe.Add(1);
            NummernListe.Add(1);
            NummernListe.Add(2);
            NummernListe.Add(3);

            List<Person> PersonenListe = new List<Person>();

            // Variante 1:
            Person Mitarbeiter = new Person() { Vorname = "Donald", Nachname = "Duck" } ;
            PersonenListe.Add(Mitarbeiter);

            // Variante 2: 
            PersonenListe.Add(new Person() { Vorname = "Donald", Nachname = "Duck" });

            // Variante 3: AddRange
            List<Person> tmpPersonenListe = new List<Person>();
            tmpPersonenListe.Add(new Person() { Vorname = "Dasy", Nachname = "Duck" });
            tmpPersonenListe.Add(new Person() { Vorname = "Daniel", Nachname = "Düsentrieb" });

            PersonenListe.AddRange(tmpPersonenListe);

            #endregion

            #region "HashSet"
            // Einsatzgebiet:
            // Überall, wo schnelle Abfragen auf Listen gemacht werden müssen.
            // Dies ist die Alternative für List<T> - bietet mehr Möglichkeiten zum Abfragen von Listen. 
            // 
            // Vorteile:
            // Verbesserte Methoden zum Suchen, Einfügen, Sortieren und Manipulieren der Daten verfügbar.
            // Wenn keine Objekte als Liste verwendet werden, kann hier geschickt vermieden werden, dass 
            // doppelte Werte sich innerhalb der Liste ablegen.
            // 
            // Nachteile:
            // Das Einfügen von Elementen ist langsamer als die einer List<T>
            HashSet<int> NummernHashSet = new HashSet<int>();
            NummernHashSet.Add(1);
            NummernHashSet.Add(1);
            NummernHashSet.Add(2);
            

            HashSet<Person> PersonenListeHashSet = new HashSet<Person>();
            PersonenListeHashSet.Add(new Person() { Vorname = "Donald", Nachname = "Duck" });
            PersonenListeHashSet.Add(new Person() { Vorname = "Donald", Nachname = "Duck" });

            #endregion

            #region "LinkedList"
            // Einsatzgebiet:
            // Wenn man viele Insert-und Remove - Operationen innerhalb einer Liste vornehmen möchte, jedoch den 
            // indexbasierten Zugriff nicht benötigt. Mit dieser Klasse kann man eine sehr große Liste innerhalb 
            // kurzer Zeit aufbauen, ohne beim Hinzufügen von Elementen innerhalb der Liste(also z.B.an Position x) 
            // Performance einzubüßen.
            // Auch das Durchlaufen der Liste ist ausreichend performant, da jedes Element durch eine Klasse gekapselt ist, welche das vorige Element und das nächste Element kennt(Eine verkettete Liste).
            // 
            // Vorteile: 
            // Schneller Einfügevorgang an beliebiger Position und Entfernen von Knoten(beides O1).
            // Bei größeren Listen landet diese nicht auf dem LOH(Large Object Heap)
            // 
            // Nachteile:
            //             Kein Zugriff über Indexer möglich.
            // Leichter Overhead je Element, da diese in eine Klasse gekapselt werden.
            // 
            // Elemente hinzufügen:
            // AddAfter: "Fügt nach einem vorhandenen Knoten in der LinkedList einen neuen Knoten oder Wert hinzu. "
            // AddBefore: "Fügt vor einem vorhandenen Knoten in der LinkedList einen neuen Knoten oder Wert hinzu. "
            // AddFirst: "Fügt am Anfang der LinkedList einen neuen Knoten oder Wert hinzu. "
            // AddLast: "Fügt am Ende der LinkedList einen neuen Knoten oder Wert hinzu. "
            // 
            // Elemente entfernen:
            // Remove: "Entfernt das erste Vorkommen eines Knotens oder Werts aus der LinkedList. "
            // RemoveFirst: "Entfernt den Knoten am Anfang der LinkedList. "
            // RemoveLast: "Entfernt den Knoten am Ende der LinkedList. "
            LinkedList<int> NummernLinkedList = new LinkedList<int>();
            LinkedListNode<int> firstNode = NummernLinkedList.AddFirst(1);
            LinkedListNode<int> nextNode = NummernLinkedList.AddAfter(firstNode, 2);
            NummernLinkedList.AddLast(4);
            NummernLinkedList.AddBefore(nextNode, 3);

            #endregion

            #region "Queue"
            // Einsatzgebiet:
            // Immer dann, wenn Nachrichten oder Aufträge in genau der Reihenfolge abgearbeitet werden müssen, 
            // in der diese auftreten / eintreffen.
            // 
            // Beispiele die eine Synchronisierung voraussetzen(siehe auch  Simple SyncQueue):
            // JobQueue: Wenn man z.b.einen Arbeits-Thread hat, kann man ihn mithilfe einer Queue Aufträge 
            // vergeben, ohne warten zu müssen, das der Thread Zeit hat.
            // MessageQueue: Häufig in Server - Client - Systemen am Server implementiert.Dort werden 
            // alle Anfragen der Clients an den Server geschrieben, damit der Server die Anfragen sequenziell abarbeiten kann(siehe auch  Bulk Queue - Klasse und  Applikation mit Warteschlange)
            // 
            // 
            // Vorteile:
            // First in first out Prinzip.
            // Eingebaute Sequenzierung der Daten.
            // Innerhalb ihres Aufgabengebietes ist die Queue performant.
            // 
            // Nachteile:
            // Kein indexbasierter Zugriff.
            // Kein Zugriff auf beliebige Elemente innerhalb der Queue möglich(wenn 5 Elemente in der 
            // Queue sind, so muss man bei einem gewollten Zugriff auf Element Nr. 3 zuerst Element 1 und 2 von der Queue holen).
            // Kein Hinzufügen innerhalb der Queue möglich(man kann nicht an Position x (wobei x != Count) ein Element einfügen.
            // 
            // Elemente hinzufügen:
            // Enqueue: "Fügt am Ende der Queue<T> ein Objekt hinzu."
            // 
            // Elemente entfernen:
            // Dequeue: "Entfernt das Objekt am Anfang von Queue<T> und gibt es zurück."

            Queue<int> QueueList = new Queue<int>();
            QueueList.Enqueue(1);
            QueueList.Enqueue(2);
            QueueList.Enqueue(3);
            QueueList.Enqueue(4);

            Queue<Person> QueueListPerson = new Queue<Person>();
            QueueListPerson.Enqueue(new Person() { Vorname = "Donald", Nachname = "Duck" });
            QueueListPerson.Enqueue(new Person() { Vorname = "Dasy", Nachname = "Duck" });

            #endregion

            #region "Stack"
            // Einsatzgebiet:
            // Für Undo und Redo Features unerlässlich.
            // 
            // Vorteile:
            // Last in first out Prinzip.
            // Wenn Count kleiner als die Kapazität des Stapels ist, ist Push eine O(1)-Operation.Wenn die Kapazität zur Anpassung an das neue Element erhöht werden muss, wird Push zu einer O(n) - Operation, wobei n gleich Count ist.Pop ist eine O(1)-Operation. (zu Push und Pop siehe "Elemente hinzufügen/entfernen")
            // 
            // Nachteile:
            // Kein indexbasierter Zugriff.
            // Kein Zugriff auf beliebige Elemente innerhalb des Stack möglich(wenn 5 Elemente im Stack sind, so muss man bei einem gewollten Zugriff auf Element Nr. 3 zuerst Element 5 und 4 vom Stack holen).
            // 
            // Elemente hinzufügen:
            // Push: "Fügt ein Objekt am Anfang von Stack<T> ein."
            // 
            // Elemente entfernen:
            // Pop: "Entfernt das oberste Objekt aus Stack<T> und gibt es zurück."

            Stack<int> StackList = new Stack<int>();
            StackList.Push(1);
            StackList.Push(2);
            StackList.Push(3);
            int letzteZahl = StackList.Pop();


            #endregion

            #region "ObservableCollection"
            // Einsatzgebiet:
            // Bei Auflistung von (grafischen) Objekten wie z.B. ListBox, ListView, TreeView
            // 
            // Vorteile: 
            // Verhält sich wie List<T>, besitzt jedoch fallbacks für synchrone Zugriffe und vefolgt grundlegende Konzepte zur Datenbindung
            // 
            ObservableCollection<Person> ObservableCollectionList = new ObservableCollection<Person>();
            ObservableCollectionList.Add(new Person() { Vorname = "Donald", Nachname = "Duck" });
            ObservableCollectionList.Add(new Person() { Vorname = "Dasy", Nachname = "Duck" });
            ObservableCollectionList.Add(new Person() { Vorname = "Dagobert", Nachname = "Duck" });

            #endregion
        }
    }

    class Person
    {
        public String Vorname { get; set; }
        public String Nachname{ get; set; }
    }

}
