﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schleifen
{
    class Program
    {
        static void Main(string[] args)
        {
            foreachSchleifen();
            forSchleifen();
            whileSchleifen();
            VorzeitigesBeendenEinerIterativenSchleife();
            Console.ReadLine();
        }

        static void foreachSchleifen()
        {
            // An array of integers
            int[] array1 = { 0, 1, 2, 3, 4, 5 };

            foreach (int n in array1)
            {
                System.Console.WriteLine(n.ToString());
            }


            // An array of strings
            string[] array2 = { "hello", "world" };

            foreach (string s in array2)
            {
                System.Console.WriteLine(s);
            }
        }

        static void forSchleifen()
        {
            // An array of integers
            int[] array1 = { 0, 1, 2, 3, 4, 5 };

            for (int i = 0; i < 6; i++)
            {
                System.Console.WriteLine(array1[i].ToString());
            }


            // An array of strings
            string[] array2 = { "hello", "world" };

            for (int i = 0; i < 2; i++)
            {
                System.Console.WriteLine(array2[i]);
            }
        }

        static void whileSchleifen()
        {
            // An array of integers
            int[] array1 = { 0, 1, 2, 3, 4, 5 };
            int x = 0;

            #region "Kopfgesteuerte Whileschleifen"
            while (x < 6)
            {
                System.Console.WriteLine(array1[x].ToString());
                x++;
            }


            // An array of strings
            string[] array2 = { "hello", "world" };
            int y = 0;

            while (y < 2)
            {
                System.Console.WriteLine(array2[y]);
                y++;
            }

            #endregion
            #region "Fussgesteuerte Whileschleifen"

            x = 0;
            y = 0;

            do
            {
                System.Console.WriteLine(array1[x].ToString());
                x++;
            } while (x < 6);


            do
            {
                System.Console.WriteLine(array2[y]);
                y++;
            } while (y < 2);

            #endregion
        }

        static void VorzeitigesBeendenEinerIterativenSchleife()
        {
            // the good, the bad and the ugly:

            #region "the good"
            for (int counter = 1; counter <= 1000; counter++)
            {
                if (counter == 10)
                    break;
                System.Console.WriteLine(counter);
            }
            #endregion

            #region "bad and ugly"
            // the bad:
            for (int counter = 1; counter <= 1000; counter++)
            {
                if (counter == 10)
                    counter = 1001;
                System.Console.WriteLine(counter);
            }

            // the ugly
            for (int counter = 1; counter <= 1000; counter++)
            {
                if (counter == 10)
                {
                    goto Found;
                }
            }
            goto Finish;

            Found:
                System.Console.WriteLine("Gefunden");
                goto Weiter;
            Finish:
                System.Console.WriteLine("Nicht gefunden");
            Weiter:
                System.Console.WriteLine("NEVER USE GOTO HERE!!");
            #endregion
        }
    }
}
