﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerableBeispiel
{
    class Program
    {
        static int[] zufallszahlen = new[] { 5, 36, 3, -6, 2, 3, 6, 5, 9, 8 };

        static void Main(string[] args)
        {
            IEnumerable<int> allePostivenZahlen = zufallszahlen.Where( i => i>=0 );

            foreach ( int z in allePostivenZahlen)
            {
                Console.WriteLine(z);
            }

            IEnumerable<int> alleNegativenZahlen = zufallszahlen.Where(i => i < 0);

            foreach (int z in alleNegativenZahlen)
            {
                Console.WriteLine(z);
            }

            // In LINQ sähe es folgendermaßen aus:

            var MeinePositivenZahlen = 
                from i in zufallszahlen
                where i >= 0
                select i;

            Console.WriteLine("--");

            foreach (int z in MeinePositivenZahlen)
            {
                Console.WriteLine(z);
            }

            var MeinePositivenZahlen1 = zufallszahlen
                    .Where ( i => i >= 0)
                    .Select (z => z);

            Console.WriteLine("--");

            foreach (int z in MeinePositivenZahlen1)
            {
                Console.WriteLine(z);
            }

            var MeinePositvenZahlen3 = Enumerable.Where(zufallszahlen, i=> i>=0 ).Select( foo => foo);


            Console.ReadLine();
        }
    }
}
