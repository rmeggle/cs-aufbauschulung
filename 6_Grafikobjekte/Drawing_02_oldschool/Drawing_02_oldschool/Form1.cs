﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// Websource: https://msdn.microsoft.com/en-us/library/sfwzeec0(v=vs.90).aspx
/// </summary>
namespace Drawing_02_oldschool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Drawing.Graphics graphics = this.CreateGraphics();
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(10, 10, 250, 250); // x1 y1 x2 y2
            graphics.DrawEllipse(System.Drawing.Pens.Black, rectangle);
            graphics.DrawRectangle(System.Drawing.Pens.Red, rectangle);
            graphics.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Drawing.Graphics graphics = this.CreateGraphics();
            this.Invalidate(); // Forces a redraw
        }
    }
}
