﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_7_statischeKlassenMember
{
    class Program
    {
        // Grundsatz: statische Klassen können nicht instanziert werden! Der zugriff 
        // erfolgt über den Klassennamen selbst. 

        #region "Klassen"

        /// <summary>
        /// * darf selbst nur statische "Member" haben
        /// * kann nicht instanziert werden
        /// * => ist versiegelt
        /// * kann nicht mit new instanziert werden
        /// </summary>
        static class Person
        {
            /// <summary>
            /// statische Klassen sind hervorragend geeignet für Projektweite Konstanten
            /// </summary>
            public const double Pi = 3.1415926;


            /// <summary>
            /// statische Methode von Person:
            /// </summary>
            static public void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Person");
            }

        }

        class Person1
        {

            //statische Eigenschaften von Person1
            public static Boolean HatGrips { get; set; }

            // nicht statische Eigenschaften von Person1
            public String Vorname { get; set; }
            public String Nachname { get; set; }

            /// <summary>
            /// statische Methode von Mensch:
            /// </summary>
            public void Personendaten()
            {
                Console.Write(String.Format ("{0} {1} hat ", this.Vorname, this.Nachname));
                if (HatGrips)
                {
                    Console.WriteLine("Grips");
                } else
                {
                    Console.WriteLine("kein Grips");
                }
                
            }
        }

     

        #endregion

        static void Main(string[] args)
        {
            string trenner = new string('-', 45);

            // Person p1 = new Person(); // geht nicht, da statisch

            // Zugriff nur über den Klassennamen selbst:
            Person.Personendaten(); // "Personendaten aus Klasse Person"

            Person1.HatGrips = true;

            Person1 p1 = new Person1() { Vorname = "Dagobert", Nachname = "Duck" } ;
            Person1 p2 = new Person1() { Vorname = "Daniel", Nachname = "Düsentrieb" };

            p1.Personendaten(); // "Dagobert Duck hat Grips"
            p2.Personendaten(); // "Daniel Düsentrieb hat Grips"

            Console.WriteLine(trenner);

            Person1.HatGrips = false;

            Person1 p3 = new Person1() { Vorname = "Pluto", Nachname = "" };
            Person1 p4 = new Person1() { Vorname = "Donald", Nachname = "Duck" };

            p3.Personendaten(); // Pluto hat kein Grips
            p4.Personendaten(); // Donald Duck hat kein Grips

            Console.WriteLine(trenner);
            // aber nun auch: 

            p1.Personendaten(); // "Dagobert Duck hat kein Grips"
            p2.Personendaten(); // "Daniel Düsentrieb hat kein Grips"

            Console.ReadLine();
        }
    }
}
