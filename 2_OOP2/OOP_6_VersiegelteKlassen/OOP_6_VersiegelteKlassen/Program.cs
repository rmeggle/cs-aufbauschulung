﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_6_VersiegelteKlassen
{
    class Program
    {
        #region "Klassen"
        class Person
        {
            /// <summary>
            /// Methode von Person
            /// </summary>
            public virtual void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Person");
            }

        }

        /// <summary>
        /// Von der versiegelten Klasse Mitarbeiter kann nicht weiter abgeleitet werden. 
        /// Gund, warum man versiegelt: Durch Laufzeitoptimierung "etwas" schneller als wenn nicht versiegelt.
        /// </summary>
        sealed class Mitarbeiter : Person
        {
            /// <summary>
            /// Methode von Mitarbeiter überschreibt die Methode Personendaten aus Person
            /// </summary>
            public new void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Mitarbeiter");
            }
        }

        // Geht nicht - da Mitarbeiter versiegelt (sealed)
        //public class Vertrieb : Mitarbeiter
        //{
        //
        //}

        class Geschäftsleitung : Person
        {
            /// <summary>
            /// Methode von Mitarbeiter überschreibt die Methode Personendaten aus Person
            /// </summary>
            public new void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Mitarbeiter");
            }
        }

        // Geht, da Geschäftsleitung nicht versiegelt (sealed)
        class Geschäftsführer : Geschäftsleitung
        {
            /// <summary>
            /// Methode von Mitarbeiter überschreibt die Methode Personendaten aus Person
            /// </summary>
            public new void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Mitarbeiter");
            }
        }

        #endregion

        static void Main(string[] args)
        {

            string trenner = new string('-', 45);

            Person p1 = new Person();
            Mitarbeiter m1 = new Mitarbeiter();

            p1.Personendaten(); // "Personendaten aus Klasse Person"
            m1.Personendaten(); // "Personendaten aus Klasse Mitarbeiter"

            Console.WriteLine(trenner);

            Person p2 = new Mitarbeiter();
            p2.Personendaten(); // "Personendaten aus Klasse Person" (da new und nicht overwrite bei Instanzbildung)

            Mitarbeiter m2 = new Mitarbeiter();
            Person p3 = (Person)m2; // Typecast von versiegelte Klasse Mitarbeiter m2 auf Referenztyp Person 
            p3.Personendaten(); // "Personendaten aus Klasse Person" (da new und nicht overwrite bei Instanzbildung)

            Console.ReadLine();

        }
    }
}
