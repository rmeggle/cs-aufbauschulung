﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_Polimorphismus
{

    #region "Klassen"
    class Person
    {
        /// <summary>
        /// Methode von Person
        /// </summary>
        public virtual void Personendaten()
        {
            Console.WriteLine("Personendaten aus Klasse Person");
        }
    }

    class Mitarbeiter : Person
    {
        /// <summary>
        /// Methode von Mitarbeiter überschreibt die Methode Personendaten aus Person
        /// </summary>
        public override void Personendaten()
        {
            Console.WriteLine("Personendaten aus Klasse Mitarbeiter");
        }
    }

    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person();
            p1.Personendaten(); // "Personendaten aus Klasse Person"

            Mitarbeiter m1 = new Mitarbeiter();
            m1.Personendaten(); // "Personendaten aus Klasse Mitarbeiter"

            Person p2 = new Mitarbeiter();
            p2.Personendaten(); // "Personendaten aus Klasse Mitarbeiter" (immer noch - da overwrite bei Instanzbildung)

            Mitarbeiter m2 = new Mitarbeiter();
            Person p3 = (Person)m2; // Typecast von Mitarbeiter m2 auf Referenztyp Person 
            p3.Personendaten(); // "Personendaten aus Klasse Mitarbeiter" (immer noch - da overwrite bei Instanzbildung)

            // Wenn eine (abgeleitete) Klasse einen virtuellen "Member" überschreibt, wird dieser 
            // "Member" bei >>overwrite<< auch dann aufgerufen, wenn eine Instanz auf die 
            // Basisklasse zugreifen würde



            Console.ReadLine();
        }
    }
}
