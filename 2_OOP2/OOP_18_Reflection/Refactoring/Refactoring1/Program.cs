﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Refactoring1
{
    class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            var MeineEigeneAssembly = Assembly.GetExecutingAssembly();
            Console.WriteLine(MeineEigeneAssembly.FullName);

            var MeineAssemblyTypes = MeineEigeneAssembly.GetTypes();
            foreach (var t in MeineAssemblyTypes)
            {
                //Console.WriteLine(t.Name);

                //var MeineAssemblyProperties = t.GetProperties();

                //foreach ( var p in MeineAssemblyProperties)
                //{
                //    Console.WriteLine(p.Name);
                //}

            }

            // Noch keine Instanz gebildet: 
            Type ClassType = typeof(Person);
            var ClassProperties = ClassType.GetProperties().Where(x => x.CanWrite);
            foreach (var cp in ClassProperties)
            {
                Console.WriteLine(cp.Name);
                if (cp.Name == "Vorname")
                {
                    //Person.Vorname = "Donald";
                }
            }

            // Wenn ich eine Instanz gebildet habe dann: 
            Person personenObjekt = new Person();
            Type ObjectTyp = personenObjekt.GetType();
            var ObjectProperties = ObjectTyp.GetProperties().Where(x => x.CanWrite);

            foreach ( var meineProperties in ObjectProperties)
            {
                if (meineProperties.Name == "Vorname")
                {
                    personenObjekt.Vorname = "Donald";
                }
            }



            Console.ReadLine();
        }
    }
}
