﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attribute
{
    [Obsolete("Die Klasse ist veraltet und durch Mitarbeiter blah blah")]
    class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }
    }

    class Mitarbeiter
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            Mitarbeiter m = new Mitarbeiter();
            Console.ReadLine();
        }
    }
}
