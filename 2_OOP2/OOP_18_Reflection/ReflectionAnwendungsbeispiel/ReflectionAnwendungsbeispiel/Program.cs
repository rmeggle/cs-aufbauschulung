﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ReflectionAnwendungsbeispiel
{
    class Person
    {
        public void MachtMisst()
        {
            try
            {
                int divident = 0;
                int divisor = 0;
                int ergebnis = divident / divisor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Kasse: " + this.GetType().Name);
                Console.WriteLine("Methode: " + MethodInfo.GetCurrentMethod().Name);
            }
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.MachtMisst();
            Console.ReadLine();

        }
    }
}
