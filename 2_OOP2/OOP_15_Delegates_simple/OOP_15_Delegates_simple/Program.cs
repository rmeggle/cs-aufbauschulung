﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Ein Delegate ist ein typsicherer Funktionszeiger. Er zeigt (referenziert) auf eine statische Funktion innerhalb einer Klasse.
/// Die Signatur des Delegates muss mit der Signatur der Funktion übereinstimmen auf die der Delegate zeigt.
/// Generell ist ein Delegate identisch mit einer Klasse. Es kann eine Instanz erzeugt werden und der Funktionsname zeigt auf den 
/// delegate Konstruktur -> und er zeigt auf nur diese (statische) Funktion der Klasse. 
/// </summary>
namespace OOP_15_Delegates_simple
{
    /// <summary>
    /// Definieren des Delegates "SagHalloWelt". Er soll später auf die Funktion "HalloVonClassPerson" der Klasse 
    /// ClassPerson zeigen, aber auch auf die eigenen, statischen, Methoden Ausgabe1 und Ausgabe2. 
    /// 
    /// Die Funktion "ClassPerson::HalloVonClassPerson", "Program::Ausgabe1" und "Program::Ausgabe2" haben jeweils die
    /// gleiche Signatur. 
    /// </summary>
    /// <param name="Nachricht"></param>
    public delegate void SagHalloWelt(string Nachricht);

    class Program
    {

        static void Main(string[] args)
        {
            // Aufruf der Methoden Ausgabe1 und Ausgabe2 als Verweis auf den Delegate: 
            SagHalloWelt schreibe1 = new SagHalloWelt(Ausgabe1);
            schreibe1("Das ist der erste delegate-Aufruf");
            SagHalloWelt schreibe2 = new SagHalloWelt(Ausgabe2);
            schreibe2("Das ist der zweite delegate-Aufruf");


            // Zugriff auf ClassPerson durch Instanz
            ClassPerson p1 = new ClassPerson();
            p1.Vorname = "Donald";
            p1.Nachname = "Duck";

            // Bilden einer Instanz des Delegates und verweisen auf die Funktion "HalloVonClassPerson"
            // der Klasse ClassPerson
            SagHalloWelt MeinHalloWeltDelegate = new SagHalloWelt(ClassPerson.HalloVonClassPerson);

            // Aufrufen der (statischen) Funktion "HalloVonClassPerson" 
            MeinHalloWeltDelegate("Hallo Welt");

            Console.ReadLine();
        }

        public static void Ausgabe1(string Text)
        {
            Console.WriteLine("Ausgabe1 sagt {0}", Text);
        }

        public static void Ausgabe2(string Text)
        {
            Console.WriteLine("Ausgabe2 sagt {0}", Text);
        }


    }
}
