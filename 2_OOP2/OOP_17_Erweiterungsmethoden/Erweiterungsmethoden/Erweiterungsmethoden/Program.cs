﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MeineZeitmethoden;

namespace Erweiterungsmethoden
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime JetztUndHeute = DateTime.Now;
            Console.Write("Die Zeit am Bodensee:");
            Console.WriteLine(JetztUndHeute);

            // Außenstelle ist in NewYork mit einer Zeitverschiebung... 6h?
            DateTime InNewYork = JetztUndHeute.ZeitInNY();
            Console.Write("Die Zeit am Big Apple:");
            Console.WriteLine(InNewYork);
            

            Console.ReadLine();

            
        }
    }
}
