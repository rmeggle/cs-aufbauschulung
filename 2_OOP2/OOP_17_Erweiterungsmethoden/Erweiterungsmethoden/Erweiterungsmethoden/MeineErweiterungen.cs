﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeineZeitmethoden
{
    static class MeineErweiterungen
    {
        /// <summary>
        /// Eine Erweiterungsmethode (extention Method in engl.) zeichnet sich damit aus, 
        /// dass als Attribut das zu Grunde liegende Objekt übergeben wird. 
        /// </summary>
        /// <param name="AktuelleUhrzeit"></param>
        /// <returns></returns>
        public static DateTime ZeitInNY(this DateTime AktuelleUhrzeit, int Zeitverschiebung = -6)
        {
            return AktuelleUhrzeit.AddHours(Zeitverschiebung);
        }
    }
}
