﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_16_DelegatesAnwendung
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Person"></param>
    /// <returns></returns>
    delegate bool listePersonAuf(ClassPerson Person);

    class ClassPerson
    {
        #region "Eigenschaften"
        public String Vorname { get; set; }
        public String Nachname { get; set; }
        public DateTime Geburtstag { get; set; }
        #endregion

        #region "Methoden für die Verwendung OHNE Delegates"
        public static void AlleMinderjährige(List<ClassPerson> Personen)
        {
            string trenner = new string('-', 45);
            Console.WriteLine("Alle volljährige Personen: "); Console.WriteLine(trenner);
            foreach (ClassPerson p in Personen)
            {
                if (p.Geburtstag.AddYears(18) < DateTime.Now)
                {
                    Console.WriteLine("{0} {1}", p.Vorname, p.Nachname);
                }
            }
        }

        public static void AlleVolljährige(List<ClassPerson> Personen)
        {
            string trenner = new string('-', 45);
            Console.WriteLine("Alle volljährige Personen: "); Console.WriteLine(trenner);
            foreach (ClassPerson p in Personen)
            {
                if (p.Geburtstag.AddYears(18) < DateTime.Now)
                {
                    Console.WriteLine("{0} {1}", p.Vorname, p.Nachname);
                }
            }
        }
        #endregion

        #region "Methoden für die Verwendung MIT Delegates"
        public static void Personenliste(List<ClassPerson> Personen, listePersonAuf zeigePerson)
        {
            string trenner = new string('-', 45);
            foreach (ClassPerson Person in Personen)
            {
                if (zeigePerson(Person))
                {
                    Console.WriteLine("{0} {1}", Person.Vorname, Person.Nachname);
                }
}
        }        
        #endregion

    }
}
