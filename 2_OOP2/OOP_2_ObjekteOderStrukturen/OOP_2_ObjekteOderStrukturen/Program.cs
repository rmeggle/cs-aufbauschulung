﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_2_ObjekteOderStrukturen
{
    class Program
    {

        public struct Person
        {
            // Eine Struktur kann Felder haben. Sie können aber nur initialisiert werden, 
            // wenn sie als statisch oder konstant deklariert sind un einen Wert zugewiesen bekommen
            public const string publicFooFeld = "";
            private const string privateFooFeld = "";
            internal static string internalFeld = "";
            //protected static int protectedfoo = ""; // geht nicht, da Strukturen nicht ableitbar sind...

            // ... geht in dem Fall auch ohne const/static und Zuweisung, da sie im Konstruktor Person(string name) definiert wird 
            public string publicfoo;


            /// <summary>
            /// Eine Struktur kann keinen Standardkonstruktor (ein Konstruktor ohne Parameter) und keinen Destruktor deklarieren.
            /// Es kann jedoch aber Konstruktoren mit Parametern definiert werden
            /// </summary>
            /// <param name="name"></param>
            public Person(string name)
            {
                publicfoo = name;
            }
        }


        static void Main(string[] args)
        {
            Person person1 = new Person();
            Console.WriteLine(person1.publicfoo);

            Person person2 = new Person("Donald Duck");
            Console.WriteLine(person2.publicfoo);

            // Eine Struktur kann auch ohne dem Schlüsselwort "new" deklariert werden:
            Person person3;
            person3.publicfoo = "foobar"; // Es muss eine Zuweisung erfolgen, ansonsten Fehler bei Ausgabe: 
            Console.WriteLine(person3.publicfoo);

            // 1000 Personen in eine list of struct: 
            List<Person> PersonList = new List<Person>();
            for (int i=0; i<1000; i++)
            {
                Person P = new Person();
                P.publicfoo = String.Concat("Donald nr. ",i);
                PersonList.Add(P);
            }

            // Ausgabe: 
            foreach (Person P in PersonList)
            {
                Console.WriteLine(P.publicfoo);
            }

            Console.ReadLine();

            // Frage: Was passiert, wenn man aus obigen struct eine class machen will? Was muss geändert werden?
            // Hinweise: 
            // * Eine Struktur kann keinen Standardkonstruktor (ein Konstruktor ohne Parameter) und keinen Destruktor deklarieren.
            // * Strukturen können Konstruktoren deklarieren, die über Parameter verfügen.
            // * Strukturen sind Werttypen, und Klassen sind Referenztypen.
            // * Strukturen können im Gegensatz zu Klassen ohne den Operator new instanziiert werden.
        }
    }
}
