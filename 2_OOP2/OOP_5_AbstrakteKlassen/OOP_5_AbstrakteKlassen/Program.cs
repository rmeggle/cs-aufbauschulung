﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_5_AbstrakteKlassen
{
    class Program
    {
        // Grundsatz: Abstrakte Klassen dürfen nicht instanziert werden!

        #region "Klassen"
        public class Person
        {
            /// <summary>
            /// Methode von Person
            /// </summary>
            public virtual void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Person");
            }

        }

        /// <summary>
        /// Mitarbeiter erbt alles von Person. Im Wesentlichen ist es also eine Art "interface" (-> Siehe Kapitel Interface), 
        /// jedoch kann eine abstrakte Klasse auch eigene Methoden und Implementierungen mitbringen, ein Interface nicht. 
        /// </summary>
        public abstract class Mitarbeiter : Person
        {
            /// <summary>
            /// Muss genannt werden, da Geschäftsleitung die Personendaten von 
            /// Person überschreiben wird
            /// </summary>
            public override abstract void Personendaten(); // darf keinen "body" haben, da abstract!

            /// <summary>
            /// Neue Implementierung von Adressdaten in Mitarbeiter
            /// </summary>
            public void Adressdaten()
            {
                Console.WriteLine("Adressdaten von Mitarbeiter");
            }

        }

        // Geschäftsleitung erbt alles von Mitarbeiter, ergo auch alles von Person
        public class Geschäftsleitung : Mitarbeiter
        {
            /// <summary>
            /// Um "Personendaten" von "Person" zu überschreiben, muss 
            /// es eine "Personendaten" in der Klasse "Mitarbeiter" geben. 
            /// </summary>
            public override void Personendaten()
            {
                Console.WriteLine("Personendaten aus Klasse Geschäftsleitung");
            }
        }

        #endregion

        static void Main(string[] args)
        {
            Person p1 = new Person();
            //Mitarbeiter m1 = new Mitarbeiter(); // geht nicht, siehe oben:  Abstrakte Klassen dürfen nicht instanziert werden!
            Geschäftsleitung G1 = new Geschäftsleitung();
            G1.Personendaten(); // "Personendaten aus Klasse Geschäftsleitung"
            G1.Adressdaten();   // "Adressdaten von Mitarbeiter"

            Console.ReadLine();

        }
    }
}
