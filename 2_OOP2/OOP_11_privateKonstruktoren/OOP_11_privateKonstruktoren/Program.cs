﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_11_privateKonstruktoren
{

    #region "Klassen"
    class Person
    {
        /// <summary>
        /// statische Klassen sind hervorragend geeignet für Projektweite Konstanten
        /// </summary>
        public const double Pi = 3.1415926;

        /// <summary>
        /// Private Konstruktoren werden eigentlich nur verwendet, um ausdrücklich 
        /// das Erstellen einer Instanz zu verhindern. 
        /// </summary>
        private Person()
        {

        }

        /// <summary>
        /// statische Methode von Person: (Nehmen wir mal an - wir hätten in dieser
        /// Klasse ausschließlich nur statische Member...)
        /// </summary>
        static public void Personendaten()
        {
            Console.WriteLine("Personendaten aus Klasse Person");
        }

    }
    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            //Person p1 = new Person(); // Fehler, da Konstruktor mit privater Zugriffskontrolle
            Person.Personendaten(); // ok, da static member und direkter Zugriff ausdrücklich erwünscht
        }
    }
}
