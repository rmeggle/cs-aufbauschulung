﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_14_PartielleKlassen
{
    /// <summary>
    /// Es ist die partielle Klasse, welche die Erweiterungen der anderen Klasse 
    /// Person beeinhaltet.
    /// => Daher trägt diese Klasse keinen Konstruktur. 
    /// </summary>
    public partial class Person
    {
        /// <summary>
        /// Methode von Person, die in dieser Datei gerade ausgearbeitet wird...
        /// </summary>
        public void Personendaten()
        {
            Console.WriteLine("Personendaten: {0} {1}", this.Vorname, this.Nachname);
        }
    }
}
