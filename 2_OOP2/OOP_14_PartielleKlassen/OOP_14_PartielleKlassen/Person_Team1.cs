﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_14_PartielleKlassen
{
    /// <summary>
    /// Hauptklasse "Person", welche den Konstruktur beschreibt
    /// </summary>
    public partial class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }

        /// <summary>
        /// Standardkonstruktor von Person
        /// </summary>
        public Person()
        {
        }


    }
}
