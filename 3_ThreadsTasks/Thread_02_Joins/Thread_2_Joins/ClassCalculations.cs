﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_2_Joins
{
    class ClassCalculations
    {
        public static Thread t1;
        public static Thread t2;

        public static void M1()
        {
            Console.WriteLine("\nM1 beginnt mit der Arbeit:");
            for (int i = 1; i <= 3; i++)
            {
                Console.Write("(M1) ");
                Thread.Sleep(1500);
            }
            Console.WriteLine("\nM1 beendet");
        }

        public static void M2()
        {
            Console.WriteLine("\nM2 beginnt mit der Arbeit, warte aber noch auf M1:");
            // Join ist eine Syncronisationsmethode, hier aufgerufen in M2, welche nun
            // wartet bis Thread1 (hier M1) fertig ist. Erst dann macht er hier weiter. 
            t1.Join(); 
            for (int i = 1; i <= 5; i++)
            {
                Console.Write("(M2) ");
                Thread.Sleep(500);
            }
            Console.WriteLine("\nM2 beendet");
        }
    }
}
