﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadParam
{
    class Arbeiter
    {
        // Bereitstellen einer Eigenschaft
        public int AnzahlAusführungen { get; set; }

        // überladener Constructor
        public Arbeiter(int anzahl)
        {
            this.AnzahlAusführungen = anzahl;
        }

        // Arbeitercode: 
        public void arbeite()
        {
            int anzahl = this.AnzahlAusführungen;

            for (int i = 0; i < anzahl; i++)
            {
                Console.WriteLine("Arbeite...");
            }
        }


    }

    class Program
    {
        static void Main(string[] args)
        {

            Arbeiter arbeiter = new Arbeiter(5);

            Thread t1 = new Thread(arbeiter.arbeite);
            t1.Start();

            Console.ReadLine();
        }


    }
}
