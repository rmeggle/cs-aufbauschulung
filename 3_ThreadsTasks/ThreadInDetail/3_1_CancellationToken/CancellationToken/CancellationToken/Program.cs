﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CancellationTokenExample
{
    class Program
    {
        

        static void Main(string[] args)
        {

            // Initalisieren des CanncellationToken's
            var source = new CancellationTokenSource();

            try
            {
                var t1 = Task.Factory.StartNew(() => MachWas(1, 1000, source.Token))
                                     .ContinueWith((irgendwas) => MachNochmalWas(2, 1500, source.Token))
                                     .ContinueWith((irgendwas) => MachNochmalWasWeiter(3, 1500, source.Token));

                char ch = Console.ReadKey().KeyChar;
                if (ch == 'q')
                {
                    source.Cancel();
                    Console.WriteLine("Durch Anwender abgebrochen!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception aufgetreten: " + ex.Message);
            }

            // Hier drückt jemand auf "Abbrechen" => Egal wie weit wir sind, 
            // alle Tasks sollen das Signal des Abbruchs bekommen!

            Console.ReadLine();

        }

        static void MachWas(int id, int schlafeInSekunden, CancellationToken token)
        {
            // Sofern token das Signal des Abbruch trägt...
            if (token.IsCancellationRequested)
            {
                // Dann starte den Task nicht sondern "gleite" hinaus. 
                // Bitte dann try-catch beim Aufrufer beachten wenn es darum geht
                // den Grund des Abbruchs zu erfahren
                Console.WriteLine("Abbruch MachWas...");
                throw new OperationCanceledException(token);
            }

            Console.WriteLine($"MachWas Nr. {id}");
            Thread.Sleep(schlafeInSekunden);
        }

        static void MachNochmalWas(int id, int schlafeInSekunden, CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                Console.WriteLine("Abbruch MachNochmalWas...");
                throw new OperationCanceledException(token);
            }

            Console.WriteLine($"MachNochmalWas Nr. {id}");
            Thread.Sleep(schlafeInSekunden);
        }

        static void MachNochmalWasWeiter(int id, int schlafeInSekunden, CancellationToken token)
        {

            if (token.IsCancellationRequested)
            {
                Console.WriteLine("Abbruch MachNochmalWasWeiter...");
                throw new OperationCanceledException(token);
            }

            Console.WriteLine($"MachNochmalWas Nr. {id}");
            Thread.Sleep(schlafeInSekunden);
        }


    }


}
