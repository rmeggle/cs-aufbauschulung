﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks; // <-- ist schon da!

namespace ConsoleAppTasksCreate
{
    class Program
    {
        static void Main(string[] args)
        {
            // Simpel und dirket: 
            //new Task(M1).Start();

            //// Oder auch: 
            //Task t1 = new Task(M1);
            //t1.Start();

            // Start via Action-Delegator:
            Task t1_0 = new Task(new Action(M1));
            t1_0.Start();
            // anonymen delegate (Parameterangabe innerhalb von { ... } )
            Task t1_1 = new Task(delegate { M1(); });
            t1_1.Start();
            // Lambda-Expression und benanntem Methodenaufruf
            Task t1_2 = new Task(() => M1());
            t1_2.Start();
            //  Lambda-Expression und anonymen Methodenaufruf
            Task t1_3 = new Task(() => { M1(); });
            t1_3.Start();

            // Ab .NET 4.0 wurde Task.Run() eingeführt:
            Task t2 = Task.Run(new Action(M1));

            // Task.Run vs. Task.Factory.StartNew (Wann nimmt man was?)
            // msdn: https://blogs.msdn.microsoft.com/pfxteam/2011/10/24/task-run-vs-task-factory-startnew/
            // => Task t3 = Task.Run(new Action(M1));
            // ist absolut identisch mit: 
            Task t4 = Task.Factory.StartNew(new Action(M1), CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
            // => Task.Run definiert einen Task mit vordefinierten Parametern, 
            // insbesondere bei async Verfahren und auch beim parallelisieren von 
            // Tasks gibt es hier feine - aber grundlegende Unterschiede. 

            // Per lambda sind auch folgende Konstruktionen möglich: 
            Task t6 = Task.Run(() => { Console.WriteLine("Hallo Welt"); });

            // ... und damit auch eine einfache Möglichkeit der Parameterübergabe an Tasks
            Task t7 = Task.Run(() => { M2("Param1", "Param2", 4711); } );

            Task<int> t8 = Task<int>.Run(() => {
                    int ret = Addiere(1, 1);
                    return ret;
            });

            Console.WriteLine(t8.Result);

            Console.ReadLine();
        }

        static void M1()
        {
            String Ausgabe = " arbeitet...";
            Console.WriteLine(Task.CurrentId + Ausgabe);
        }

        static void M2(String p1, String p2, Int32 p3)
        {
            String Ausgabe = " arbeitet...";
            Console.WriteLine(Task.CurrentId + Ausgabe + p1 + p2 + Convert.ToString(p3));
        }

        private static object LOCK = new object();

        static int Addiere(int Zahl1, int Zahl2)
        {
            lock (LOCK)
            {
                    return Zahl1 + Zahl2;
            }
        }

    }
}
