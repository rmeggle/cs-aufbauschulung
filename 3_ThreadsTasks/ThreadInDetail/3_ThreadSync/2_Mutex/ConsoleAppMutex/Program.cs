﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppMutex
{
    class Program
    {
        private static Mutex mutex = new Mutex();
        
        public static int anzahl = 0;

        static void Main(string[] args)
        {
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }

        public static void arbeite()
        {

            // Setzen des mutex
            mutex.WaitOne();
            
            // Zur Vermeidung von DeadLocks: Dem mutex eine Timespan geben, wie lange er sich selbst sperren soll: 
            //if (!mutex.WaitOne(TimeSpan.FromSeconds(3), false))
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine("Another app instance is running. Bye!");
            //    Console.ForegroundColor = ConsoleColor.White;
            //    return;
            //}

            for (int i = 0; i < 100000; i++)
            {
                anzahl++;
            }

            // freigeben des mutex
            mutex.ReleaseMutex();
        }


    }
}
