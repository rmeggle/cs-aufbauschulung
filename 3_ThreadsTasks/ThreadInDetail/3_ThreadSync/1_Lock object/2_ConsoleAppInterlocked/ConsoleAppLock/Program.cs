﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppLock
{
    class Program
    {
        public static int anzahl= 0;

        public static void arbeite()
        {
            for (int i = 0; i < 100000; i++)
            {
                // das Increment (++) und Decrement (--) ist nicht Threadsave, da bei diesem Vorgang
                // - der Wert aus einer(!) Instanz oder Register geholt wird
                // - dieser(!) Wert nun incrementiert oder decrementiert wird / nur welcher wann?
                // - dieser(!) Wert wieder in die Instanz/Register zurückgeschrieben werden. 
                // Lösung: Interlocked.Increment oder Interlocked.Decrement: 
                Interlocked.Increment(ref anzahl);
                // Mehr unter: https://docs.microsoft.com/de-de/dotnet/api/system.threading.interlocked?view=netframework-4.8
            }
        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(arbeite);
            Thread t2 = new Thread(arbeite);
            Thread t3 = new Thread(arbeite);
            t1.Start();
            t2.Start();
            t3.Start();
            Console.WriteLine("Gesamtanzahl : " + anzahl);

            Console.ReadLine();
        }
    }
}
