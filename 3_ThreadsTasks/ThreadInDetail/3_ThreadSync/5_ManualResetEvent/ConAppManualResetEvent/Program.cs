﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConAppManualResetEvent
{
    class Program
    {
        // AutoResetEvent(true) würde gelich einen gesetzten Set() entsprechen
        public static ManualResetEvent manualresetevent = new ManualResetEvent(false);

        static void Robin1()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...gestartet");

            Thread.Sleep(1000);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...der andere Thread kann weitermachen");

            manualresetevent.Set(); // Setze das AutoResetEvent

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...warte nun selbst auf das AutoResetEvent");


            manualresetevent.WaitOne(); // Ab hier wartet er auf das Set()

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...fertig");
            Console.ForegroundColor = ConsoleColor.White;
        }

        static void Robin2()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Thread.CurrentThread.Name + "...gestartet");
            Console.WriteLine(Thread.CurrentThread.Name + "...warte nun auf auf das AutoResetEvent");

            manualresetevent.WaitOne(); // Ab hier wartet er auf das Set()


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Thread.CurrentThread.Name + "...AutoResetEvent ist gesetzt, ich kann weitermachen!");

            manualresetevent.Set(); // Setze das AutoResetEvent

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Thread.CurrentThread.Name + "...fertig");
            Console.ForegroundColor = ConsoleColor.White;

            // Wer auch immer auf mich waret, der kann nun fertig machen
            manualresetevent.Reset();
        }

        static void Main(string[] args)
        {
            Thread r1 = new Thread(Robin1);
            Thread r2 = new Thread(Robin2);
            r1.Name = "Robin 1";
            r2.Name = "Robin 2";
            r1.Start();
            r2.Start();

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }
    }
}
