﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppThreadMonitor
{
    class Program
    {
        static object sperre = new object();

        public static int Zähler = 0;

        public static void dividiere()
        {
            Monitor.Enter(sperre);
            try
            {
                // Zähler von -10 bis +10 :
                for (int Nenner = -10; Nenner < 10; Nenner++)
                {
                    Zähler++;
                    // bei i==0 wird es zu einer exception kommen: 
                    double Ergebnis = Zähler / Nenner;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("{0} dividiert durch {1} ist {2} " , Zähler,Nenner, Ergebnis);
                    Console.ForegroundColor = ConsoleColor.White;

                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Fehler aufgetreten: " + ex.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
            finally
            {
                Monitor.Exit(sperre);
            }

        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(dividiere);
            Thread t2 = new Thread(dividiere);
            Thread t3 = new Thread(dividiere);
            t1.Start();
            t2.Start();
            t3.Start();
            Console.ReadLine();
        }
    }
}
