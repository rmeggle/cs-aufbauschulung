﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppSemaphore
{
    class Program
    {
        /// <summary>
        /// Es gibt SemaphoreSlim und Semaphore. SemaphoreSlim wurde in .NET 4 eingeführt. 
        /// Der Unterschied ist der weitaus geringere Ressourcenverbrauch im gegensatz 
        /// zu ursprünglichen Semaphore.
        /// 
        /// Eine Semaphore hat im Gegensatz zu lock/Monitor keinen Besitzer. Somit
        /// kann der Semaphore mitgeteilt werden, wie viele gleichzeitige Zugriffe wir
        /// auf einen Codeabschlitt zulassen wollen. 
        /// </summary>

        static Semaphore semaphore = new Semaphore(1,1); // nur ein Thread darf hinein!

        // SemaphoreSlim wurde erst später eingeführt: Ist ein wenig Schneller (sagt man...)
        // static SemaphoreSlim semaphore = new SemaphoreSlim(1); // nur ein Thread darf hinein!

        public static int NumberOfThred = 1;

        static void Main(string[] args)
        {
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();

            Console.ReadLine();
        }

        public static void arbeite()
        {
            // semaphore.Wait(); //semaphore.Wait() gibt es nur unter SemaphoreSlim
            semaphore.WaitOne(); // semaphore.WaitOne() gibt es nur unter Semaphore

            Console.Write("Thread Nr {0}:", NumberOfThred);
            NumberOfThred++;

            for (int i = 0; i < 10; i++)
            {
                Console.Write(" {0} ", i);
            }
            Console.WriteLine();

            semaphore.Release();
        }
    }
}
