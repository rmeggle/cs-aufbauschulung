﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Threads_08_ThreadPools
{
    class ClassTaskInfo
    {
        public String TaskTextInfo { get; set; }
        public int Value { get; set; }

        public ClassTaskInfo(string text, int number)
        {
            TaskTextInfo = text;
            Value = number;
        }
    }
}
