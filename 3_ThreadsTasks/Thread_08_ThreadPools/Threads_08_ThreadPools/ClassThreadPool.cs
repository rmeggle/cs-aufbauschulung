﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads_08_ThreadPools
{
    static class ClassThreadPool
    {
        static public void RunWithThreadPool()
        {
            for (int i = 0; i <= 10; i++)
            {
                // Mit CallBack:
                ClassTaskInfo TaskInformation = new ClassTaskInfo("Arbeite Task " + Convert.ToString(i) + " ab", i);
                ThreadPool.QueueUserWorkItem(new WaitCallback(CallBackRun), TaskInformation);

                // Simpler Aufruf, ohne Callback: 
                //ThreadPool.QueueUserWorkItem(Run);
            }
        }

        static public void RunWithOutThreadPool()
        {
            for (int i = 0; i <= 10; i++)
            {
                Thread obj = new Thread(Run);
                obj.Start();
            }
        }

        static public void CallBackRun(object callback)
        {
            ClassTaskInfo ti = (ClassTaskInfo)callback;
            Console.WriteLine(ti.TaskTextInfo, ti.Value);
        }

        static public void Run(object callback)
        {
            // Tasks...
        }
    }
}
