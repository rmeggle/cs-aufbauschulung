﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Threadpools sind Auflistungen von Threads, mit der verschiedene Aufgaben im Hintergrund
/// ausgeführt werden können. Threadpools werden häufig in Serveranwendungen verwendet. Jede
/// eingehende Anfrage wird ein Thread aus einem Pool zugewiesen. Wenn dieser seine Aufgabe
/// beendet hat, wird er wieder in die Warteschlange eingereiht. 
/// Diese Threadpools haben i.d.R. eine maximale Anzahl von Threads. Wenn alle aktiv sind, 
/// werden weitere Aufgaben in eine Warteschlange gestellt, bis sie wieder von einem 
/// verfügbaren Thread verarbeitet werden können. 
/// 
/// Zudem verfügen Threadpools über eine weitaus bessere Performance.
/// </summary>
namespace Threads_08_ThreadPools
{
    class Program
    {
        static void Main(string[] args)
        {

            Stopwatch mywatch = new Stopwatch();


            Console.WriteLine("Mit ThreadPool");

            mywatch.Start();
            ClassThreadPool.RunWithThreadPool();
            mywatch.Stop();

            Console.WriteLine("Verstrichene Zeit mit ThreadPools : " + mywatch.ElapsedTicks.ToString() + "ms");
            mywatch.Reset();


            Console.WriteLine("Ohne ThreadPool");

            mywatch.Start();
            ClassThreadPool.RunWithOutThreadPool();
            mywatch.Stop();

            Console.WriteLine("Verstrichene Zeit ohne ThreadPools  : " + mywatch.ElapsedTicks.ToString() + "ms");

            Console.ReadLine();
        }

    }
}
