﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_05_DeadLock_einfach
{
    class Program
    {
        static object lock1 = new object();
        static object lock2 = new object();

        static void M1()
        {
            lock (lock1)
            {
                Console.WriteLine(Thread.CurrentThread.Name + " in M1");
                // Kurz schlafen, so dass beide Threads ihren "eigenen" synchronisierten Block un-
                // gestört betreten können.Anschließend versuchen beide, in den jeweiligen fremden Block zu gelangen, 
                // --> und das Programm hängt fest
                Thread.Sleep(100); 
                Console.WriteLine(Thread.CurrentThread.Name + " möchte M2 aufrufen");
                M2();
            }
        }

        static void M2()
        {
            lock (lock2)
                {
                Console.WriteLine(Thread.CurrentThread.Name + " in M2");
                // Kurz schlafen, so dass beide Threads ihren "eigenen" synchronisierten Block un-
                // gestört betreten können.Anschließend versuchen beide, in den jeweiligen fremden Block zu gelangen, 
                // --> und das Programm hängt fest
                Thread.Sleep(100);
                Console.WriteLine(Thread.CurrentThread.Name + " möchte M1 aufrufen");
                M1();
            }
        }

        /// <summary>
        /// Ein Deadlock liegt vor, wenn zwei Threads gleichzeitig versuchen eine Ressource zu sperren, die der jeweils
        /// andere Prozess bereits aber schon gesperrt hat. Da sich dies dann nicht selbst löst, kann keiner der beiden 
        /// Threads fortgesetzt werden. 
        /// Dieses wird in diesem Beispiel mit einem sehr ungeschickten lock simuliert.
        /// Besser wäre hier, ein "if (Monitor.TryEnter(OBJ, 300))" statt "lock(OBJ) zu verwenden, welches die Sperrung nach (hier) 300ms wieder auflöst
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(M1));
            Thread t2 = new Thread(new ThreadStart(M2));
            t1.Name = "T1"; t1.Start();
            t2.Name = "T2"; t2.Start();
        }
    }
}
