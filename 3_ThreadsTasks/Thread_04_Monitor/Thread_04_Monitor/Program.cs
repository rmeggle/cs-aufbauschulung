﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Monitor bietet einen Mechanismus, der den Zugriff auf Objekte synchronisiert. 
/// Mit Hilfe von Monitor kann sichergestellt werden, dass dass mehrere Threads 
/// gleichzeitig einen bestimmten Codeteil im Programm durchlaufen, aber immer nur einer 
/// das Codesegment aktiv durchlaufen kann. Die anderen müssen solange warten. 
/// </summary>
namespace Thread_04_Monitor
{
    class Program
    {
        static readonly object isThreadLocked = new object();

        public static void Main(string[] args)
        {

            Thread[] Threads = new Thread[10];
            for (int i = 0; i < 10; i++)
            {
                Threads[i] = new Thread(new ThreadStart(PrintNumbers));
                Threads[i].Name = "Thread Nr: " + i;
            }
            foreach (Thread t in Threads)
                t.Start();

            Console.ReadLine();
        }

        public static void PrintNumbers()
        {
            Monitor.Enter(isThreadLocked);
            try
            {
                Console.Write(Thread.CurrentThread.Name + ": ");
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(100);
                    Console.Write(i + ",");
                }
                Console.WriteLine();
            }
            finally
            {
                Monitor.Exit(isThreadLocked);
            }
        }
    }
}
