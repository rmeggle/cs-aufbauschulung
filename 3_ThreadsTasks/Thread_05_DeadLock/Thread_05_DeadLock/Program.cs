﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
/// <summary>
///    .----------.              .----------.
///   | Thread 1 |\            /| Thread 2 |
///   '----------' \          / '----------'
///         |       \        /        |
///         |        \      /         |
///         |         \    /          |
///         |          \  /           |
///         |           \/            |
///         |           /\            |
///         |          /  \           |
///         |         /    \          |
///         v        /      \         v
///  .-------------.v        \ .-------------.
///  | Ressource 1 |          v| Ressource 2 |
///  '-------------'           '-------------'
/// Ein DeadLock entsteht, wenn zwei Prozesse/Threads exklusiv 
/// eine Ressource geöffnet haben, diese aber nicht schließen und
/// jeweils versuchen die Ressource des anderen zu öffnen. 
/// </summary>
namespace Thread_05_DeadLock
{
    class Program
    {

        public static void Main(string[] args)
        {

            Console.WriteLine(">>>>> START <<<<<<");
            ClassAccount accountA = new ClassAccount(4711, 0815);
            ClassAccount accountB = new ClassAccount(4711, 0815);

            var KontoManagerA = new KontoManager(accountA, accountB, 1000);
            var t1 = new Thread(KontoManagerA.Überweisung);
            t1.Name = "Thread nr 1";

            var KontoManagerB = new KontoManager(accountB, accountA, 2000);
            var t2 = new Thread(KontoManagerB.Überweisung);
            t2.Name = "Thread nr 2";

            t1.Start();

            t2.Start();

            t1.Join();
            t2.Join();
            Console.WriteLine(">>>>> END <<<<<<");
            Console.ReadLine();
        }

    }
}
