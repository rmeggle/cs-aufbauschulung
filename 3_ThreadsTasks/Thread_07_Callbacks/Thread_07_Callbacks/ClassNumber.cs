﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_07_Callbacks
{
    public delegate void SumOfNumbersCallback(int sumOfNumbers);

    class ClassNumber
    {
        // The traget number this class needs to compute the sum of numbers
        int target;

        // Delegate to call when the the Thread function completes                 
        // computing the sum of numbers
        SumOfNumbersCallback MycallbackMethod;

        // Constructor to initialize the target number and the callback delegateinitialize
        public ClassNumber(int target, SumOfNumbersCallback callbackMethod)
        {
            this.target = target;
            this.MycallbackMethod = callbackMethod;
        }

        // This thread function computes the sum of numbers and then invokes
        // the callback method passing it the sum of numbers
        public void ComputeSumOfNumbers()
        {
            int sum = 0;
            for (int i = 1; i <= target; i++)
            {
                sum = sum + i;
            }

            // Wenn Objekt "_callbackMethod" existiert, dann verwende es:
            MycallbackMethod?.Invoke(sum);
            //  _callbackMethod?.Invoke(sum); ist die Kurzschreibweise von: 
            //if (_callbackMethod != null)
            //{
            //    _callbackMethod(sum);
            //}
        }

    }
}
