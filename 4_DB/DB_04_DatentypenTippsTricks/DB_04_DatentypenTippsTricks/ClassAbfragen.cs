﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// SQL Server data type          CLR data type (SQL Server)    CLR data type (.NET Framework)  
///varbinary                     SqlBytes, SqlBinary           Byte[]  
///binary                        SqlBytes, SqlBinary           Byte[]  
///varbinary(1), binary(1)       SqlBytes, SqlBinary           byte, Byte[] 
///image                         None                          None
///
///varchar                       None                          None
///char                          None                          None
///nvarchar(1), nchar(1)         SqlChars, SqlString           Char, String, Char[]     
///nvarchar                      SqlChars, SqlString           String, Char[] 
///nchar                         SqlChars, SqlString           String, Char[] 
///text                          None                          None
///ntext                         None                          None
///
///uniqueidentifier              SqlGuid                       Guid 
///rowversion                    None                          Byte[]  
///bit                           SqlBoolean                    Boolean 
///tinyint                       SqlByte                       Byte 
///smallint                      SqlInt16                      Int16  
///int                           SqlInt32                      Int32  
///bigint                        SqlInt64                      Int64 
///
///smallmoney                    SqlMoney                      Decimal  
///money                         SqlMoney                      Decimal  
///numeric                       SqlDecimal                    Decimal  
///decimal                       SqlDecimal                    Decimal  
///real                          SqlSingle                     Single  
///float                         SqlDouble                     Double  
///
///smalldatetime                 SqlDateTime                   DateTime  
///datetime                      SqlDateTime                   DateTime 
///
///sql_variant                   None                          Object  
///User-defined type(UDT)        None                          user-defined type     
///table                         None                          None 
///cursor                        None                          None
///timestamp                     None                          None 
///xml                           SqlXml                        None
/// </summary>
namespace DB_04_DatentypenTippsTricks
{
    class ClassAbfragen
    {
        const String connString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CSharpSchulung;Data Source=localhost";

        /// <summary>
        /// Symptomatik: Client- und Serverbetriebsystem haben nicht das gleiche CultureFlag (de-de vs. en-en etc.)
        /// oder das Datum, nach dem geprüft wird, ist von einem anderen Format mit dem nicht mit dem DateTime Datentyp
        /// verglichen werden kann.
        /// Referenz T-SQL CAST und CONVERT:
        /// https://docs.microsoft.com/en-us/sql/t-sql/functions/cast-and-convert-transact-sql
        /// </summary>
        public static void DasDateTimeCultureProblem()
        {

            //Datum, das Abgefragt wird, ist im de-de Cultureflag (104) - Der SQL-Server verwendet jedoch ISO8601: 
            String AbfrageDatum = "05.07.2017";

            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL;

                    //SqlDQL = @"SELECT [EinDateTime] FROM [dbo].[tbl_DatentypBeispiel] WHERE [EinDateTime] = @AbfrageDatum";
                    SqlDQL = @"SELECT [EinDateTime] FROM [dbo].[tbl_DatentypBeispiel] WHERE CONVERT(date,[EinDateTime],126) = CONVERT(datetime,@AbfrageDatum,104)";

                    DateTime SQLErgebnis;

                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {
                        cmd.Parameters.Add("@AbfrageDatum", System.Data.SqlDbType.DateTime);
                        cmd.Parameters["@AbfrageDatum"].Value = AbfrageDatum;


                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToDateTime(reader["EinDateTime"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
        }

        /// <summary>
        /// Symptomatik: Der SQL-Server unterstützt Millisekunden, .net aber nicht. Wenn nun aber Zeitstempel per SQL zwischen
        /// Client und Server verglichen werden wollen, so bedarf es hier eines kleinen Tricks. 
        /// </summary>
        public static void DasDateTimeProblem()
        {
            //DateTime in .NET kennt keine Millisekunden: 2017-07-05 11:04:41.550 <=> 2017-07-05 11:04:41.550

            DateTime AbfrageDatum = new DateTime(2017, 5, 7, 11, 04, 41, 550);

            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL;

                    SqlDQL = @"SELECT [EinDateTime] FROM [dbo].[tbl_DatentypBeispiel] WHERE [EinDateTime] = @AbfrageDatum";

                    // Hinweis: Das Beispiel ist etwas "getrickst": Wenn ein DateTime generiert wird, hat man es im Format 104 und nicht in ISO8601. 
                    // Ausser: Wenn man es zuvor von der Datenbank abfrägt. In dem Fall:
                    SqlDQL = @"SELECT [EinDateTime] FROM [dbo].[tbl_DatentypBeispiel] WHERE DateAdd(ms,-DatePart(ms,EinDateTime),EinDateTime)=DateAdd(ms,-DatePart(ms,@AbfrageDatum),@AbfrageDatum)";
                    
                    //SqlDQL = @"SELECT [EinDateTime] FROM [dbo].[tbl_DatentypBeispiel] WHERE DateAdd(ms,-DatePart(ms,EinDateTime),EinDateTime)=DateAdd(ms,-DatePart(ms,EinDateTime),EinDateTime)";

                    DateTime SQLErgebnis;

                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {
                        cmd.Parameters.Add("@AbfrageDatum", System.Data.SqlDbType.DateTime);
                        cmd.Parameters["@AbfrageDatum"].Value = AbfrageDatum;


                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToDateTime(reader["EinDateTime"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
        }

        /// <summary>
        /// Symptomatik: Ein Recordset liefert NULL-Werte für bestimmte Spalten
        /// </summary>
        public static void DasNULLProblem()
        {


            // Lesen von NULL Werten ( insbesondere wenn es Strings sind)
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL;

                    String SQLErgebnis;  // Jetzt wäre es schön, wenn String auch ein Nullabletype wäre: String? gibt es aber nicht. 

                    SqlDQL = @"SELECT [Vorname] FROM [dbo].[tbl_DatentypBeispiel] WHERE [id] = 6";

                    // Lösung: 
                    SqlDQL = @"SELECT IsNull([Vorname],'') AS Vorname FROM [dbo].[tbl_DatentypBeispiel] WHERE [id] = 6";

                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {

                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["Vorname"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }

            // Schreiben von NULL Werten in die Datenbank
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDML;
                    SqlDML = @"INSERT INTO [dbo].[tbl_TempData] ([data]) VALUES (@WERT)";
                    String SQLErgebnis;

                    using (SqlCommand cmd = new SqlCommand(SqlDML, conn))
                    {
                        cmd.Parameters.Add("@WERT", System.Data.SqlDbType.NVarChar);
                        cmd.Parameters["@WERT"].Value = DBNull.Value;


                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["data"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }

        }


        public static void DasBoolscheProblem()
        {
            // Lesen von NULL Werten ( insbesondere wenn es Strings sind)
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL1;
                    String SqlDQL2;

                    Boolean Filter = false;

                    String SQLErgebnis;  // Jetzt wäre es schön, wenn String auch ein Nullabletype wäre: String? gibt es aber nicht. 

                    #region "Anwenden von boolsche .NET Ausdrücke auf den SQL-Datentyp: Bit"

                    SqlDQL1 = @"SELECT [Vorname] FROM [dbo].[tbl_DatentypBeispiel] WHERE [EinFlagBit] = @Filter";

                    using (SqlCommand cmd = new SqlCommand(SqlDQL1, conn))
                    {

                        cmd.Parameters.Add("@Filter", System.Data.SqlDbType.Bit);
                        cmd.Parameters["@Filter"].Value = Filter;

                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["Vorname"]);
                                Console.WriteLine("DasBoolscheProblem:" + SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }

                    #endregion

                    #region "Anwenden von boolsche .NET Ausdrücke auf den SQL-Datentyp: TinyInt"

                    SqlDQL2 = @"SELECT [Vorname] FROM [dbo].[tbl_DatentypBeispiel] WHERE [EinFlagTinyInt] = @Filter";

                    using (SqlCommand cmd = new SqlCommand(SqlDQL2, conn))
                    {

                        cmd.Parameters.Add("@Filter", System.Data.SqlDbType.TinyInt);
                        cmd.Parameters["@Filter"].Value = Filter;

                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["Vorname"]);
                                Console.WriteLine("DasBoolscheProblem (TinyInt):" + SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();

                    #endregion

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
    }
}
