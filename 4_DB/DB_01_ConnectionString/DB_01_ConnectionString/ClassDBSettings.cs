﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace DB_01_ConnectionString
{
    class ClassDBSettings
    {
        /// <summary>
        /// Um einen Connection-String zu erhalten, kann man eine Textdatei mit der Dateiendung "udl" anlegen und den Assistenten verwenden.
        /// Eine weitere wertvolle Hilfe ist die Webseite: https://www.connectionstrings.com/
        /// 
        /// Die Werte des Connection-Strings können dann folgendermaßen im Quellcode behandelt werden. Entweder: 
        /// a) trägt man statisch in den Quellcode ein, oder
        /// b) In der Datei "App.Config" anlegen
        ///     1. Solution-Explorer: References -> Add Reference -> Check: System.Configuration
        ///     2. Menü: Projekt->Eigenschaften->Einstellungen->Name=connString=>Typ(Verbindungszeichenfolge)=>Liste::Bereich "Anwendungen" => Wert eingeben
        /// c) Man verwendet die Klasse ConnectionStringBuilder aus System.Data.SqlClient
        /// </summary>
        /// <returns></returns>
        public static string MyConnectionString()
        {
            // Methode a):
            String connString =  @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=test;Data Source=localhost";

            // Methode b):
            var appSettings = ConfigurationManager.AppSettings;

            // Methode c):
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder();
            connStringBuilder["Data Source"] = "localhost";
            connStringBuilder["integrated Security"] = "SSPI";
            connStringBuilder["Initial Catalog"] = "test";

            return connString;
            
        }

    }
}
