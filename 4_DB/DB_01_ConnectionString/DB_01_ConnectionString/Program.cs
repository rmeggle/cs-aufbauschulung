﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_01_ConnectionString
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(ClassDBSettings.MyConnectionString()))
            {
                try
                {
                    conn.Open();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex); ;
                }

            }

        }
            
    }
}
