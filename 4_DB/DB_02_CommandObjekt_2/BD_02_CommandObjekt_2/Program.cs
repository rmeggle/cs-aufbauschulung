﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


/// <summary>
/// Das SqlCommand kann mit verschiedenen Methoden verwendet werden: 
///
/// ExecuteReader       -> Bei Abfragen mit SELECT -> Es liefert ein Recordset zurück
/// ExecuteNonQuery     -> Bei Absetzen nur eines SQL-Befehls, ohne Verwendung etwaiger Rückmeldung
/// ExecuteScalar       -> Nur ein einziger wert (ein Scalar, kein Recordset) wird zurück gegeben (last inserted ID, Ergebnis von Sum/Count etc.)
/// </summary>
namespace BD_02_CommandObjekt_2
{
    class Program
    {
        static void Main(string[] args)
        {
            String connString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CSharpSchulung;Data Source=localhost";

            
            

            #region "DQL: DataQueryLanguage (SELECT)"
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL = @"SELECT [data] FROM [dbo].[tbl_TempData]";
                    String SQLErgebnis;
                    
                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["data"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
            #endregion

            #region "DML: DataManipulationLanguage (INSERT/UPDATE/DELETE) mit ExecuteNonQuery"
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDML = @"INSERT INTO [dbo].[tbl_TempData]([data]) VALUES ('Test') ";
                    using (SqlCommand cmd = new SqlCommand(SqlDML, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }

                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
            #endregion

            #region "DML: DataManipulationLanguage (INSERT/UPDATE/DELETE) mit ExecuteScalar"
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDML = @"INSERT INTO [dbo].[tbl_TempData]([data]) VALUES ('Test') SELECT SCOPE_IDENTITY() ";
                    int LastInsertedID;
                    using (SqlCommand cmd = new SqlCommand(SqlDML, conn))
                    {
                        LastInsertedID = Convert.ToInt32(cmd.ExecuteScalar());
                        Console.WriteLine("Letzte ID von tbl_TempData: " + LastInsertedID);
                    }

                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
            #endregion

            Console.ReadLine();

        }
    }
}
