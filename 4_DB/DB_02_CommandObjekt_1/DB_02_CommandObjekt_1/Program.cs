﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DB_02_CommandObjekt_1
{
    class Program
    {
        static void Main(string[] args)
        {
            String connString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CSharpSchulung;Data Source=localhost";
            String Sql = "SELECT 1+1 as Ergebnis";
            int SQLErgebnis = 0;

            #region "SqlConnection / SqlCommand"
            try
            {
                SqlConnection conn = new SqlConnection(connString);

                conn.Open();
                SqlCommand cmd = new SqlCommand(Sql, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        SQLErgebnis = Convert.ToInt32(reader["Ergebnis"]);
                    }
                }
                conn.Close();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex); 
            }

            #endregion


            #region "SqlConnection / SqlCommand mit Hilfe des Schlüssekwortes 'using'"
            /*
             * Durch die Verwendung von using wird ein Gültigkeitsbereich für "conn" festgelegt. 
             * Sobald der using-Block verlassen wird, wird durch internen Aufruf des "IDisposable" Objektes
             * die Variable wieder freigegeben. Das hat verschiedene Vorteile: 
             * - höhere Lesbarkeit: auch wenn man using nicht kennt, so erschließt es sich was gemeint ist
             * - vermeiden von Fehlern: Es wird nicht mehr vergessen, Variablen/Bereiche/connections etc. zu schließen
            */
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(Sql, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToInt32(reader["Ergebnis"]);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex); 
                }

            }
            #endregion
        }
    }
}
