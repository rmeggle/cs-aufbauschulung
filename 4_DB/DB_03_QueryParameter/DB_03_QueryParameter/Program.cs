﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Bei der Suche nach einem bestimmten Datensatz oder DataSet wird in SQL die WHERE - Klausel verwendet. 
/// Demnach muss der SQL-Befehl, welcher in einem String liegt, einen Parameter verwarbeiten. 
/// </summary>
namespace DB_03_QueryParameter
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassAbfragen Abfrage = new ClassAbfragen();

            Abfrage.AbfrageMitStringManipulation("test");
            // SQL-Injektion: 
            Abfrage.AbfrageMitStringManipulation("test';DELETE FROM [data] WHERE ID=1;SELECT '");

            
            Abfrage.AbfrageMitSqlClientDatenproviderAddWithValue("test");
            // Versuch einer SQL-Injektion, welcher nun nicht mehr gelingt: 
            Abfrage.AbfrageMitSqlClientDatenproviderAddWithValue("test';DELETE FROM [data] WHERE ID=2;SELECT '");
            
            Abfrage.AbfrageMitSqlClientDatenproviderAdd("test");

        }
    }
}
