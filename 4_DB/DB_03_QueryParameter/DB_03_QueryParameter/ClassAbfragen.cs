﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_03_QueryParameter
{
    class ClassAbfragen
    {

        const String connString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=CSharpSchulung;Data Source=localhost";

        public void AbfrageMitStringManipulation(String Filter)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL;
                    SqlDQL = @"SELECT [data] FROM [dbo].[tbl_TempData] WHERE [data] = '" + Filter + "'";
                    SqlDQL = String.Format(@"SELECT [data] FROM [dbo].[tbl_TempData] WHERE [data] = '{0}'", Filter);
                    String SQLErgebnis;

                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["data"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
        }

        public void AbfrageMitSqlClientDatenproviderAddWithValue(String Filter)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL;
                    SqlDQL = @"SELECT [data] FROM [dbo].[tbl_TempData] WHERE [data] = @Filter";
                    String SQLErgebnis;

                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {

                        cmd.Parameters.AddWithValue("@Filter", Filter);

                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["data"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }
        }

        public void AbfrageMitSqlClientDatenproviderAdd(String Filter)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    conn.Open();
                    String SqlDQL;
                    SqlDQL = @"SELECT [data] FROM [dbo].[tbl_TempData] WHERE [data] = @Filter";
                    String SQLErgebnis;

                    using (SqlCommand cmd = new SqlCommand(SqlDQL, conn))
                    {
                        cmd.Parameters.Add("@Filter", System.Data.SqlDbType.NVarChar);
                        cmd.Parameters["@Filter"].Value = Filter;


                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                SQLErgebnis = Convert.ToString(reader["data"]);
                                Console.WriteLine(SQLErgebnis);
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }

            }

        }
    }
}
