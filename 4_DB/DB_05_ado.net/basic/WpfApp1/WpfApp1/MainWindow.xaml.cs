﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Erstellen eines eigenen DataSets (ohne Datenbankverbindung)
            DataSet MeineDatenbank = new DataSet("MeineDatenbank");
            // Erstellen der Tabelle tbk_Mitarbeiter:
            DataTable tbl_Mitarbeiter = new DataTable("tbl_Mitarbeiter");
            tbl_Mitarbeiter.Columns.Add("id", typeof(Int32));
            tbl_Mitarbeiter.Columns.Add("Vorname", typeof(String));
            tbl_Mitarbeiter.Columns.Add("Nachname", typeof(String));
            tbl_Mitarbeiter.PrimaryKey = new DataColumn[] { tbl_Mitarbeiter.Columns["id"] };

            // Erstellen der Tabelle tbl_Gehälter:
            DataTable tbl_Gehälter = new DataTable("tbl_Gehälter");
            tbl_Gehälter.Columns.Add("id", typeof(Int32));
            tbl_Gehälter.Columns.Add("MitarbeiterID", typeof(Int32));
            tbl_Gehälter.Columns.Add("Gehalt", typeof(Double));
            tbl_Gehälter.Columns.Add("Datum", typeof(DateTime));

            // Die Tabellen der Datenbank zuordnen
            MeineDatenbank.Tables.Add(tbl_Mitarbeiter);
            MeineDatenbank.Tables.Add(tbl_Gehälter);

            // PK und FK Beziehungen herstellen
            DataRelation MitarbeiterGehaltRelation = MeineDatenbank.Relations.Add("relMitarbeiterGehälter",
                MeineDatenbank.Tables["tbl_Mitarbeiter"].Columns["id"],
                MeineDatenbank.Tables["tbl_Gehälter"].Columns["MitarbeiterID"]);


            // Tabelle füllen: 
            tbl_Mitarbeiter.Rows.Add("1", "Donald", "Duck");
            tbl_Mitarbeiter.Rows.Add("2", "Daniel", "Düsentrieb");
            // Gehälter:
            for (int m = 1; m < 12; m++)
            {
                tbl_Gehälter.Rows.Add(Convert.ToString(m), "1", 2500, new DateTime(2017, m, 1));
            }
            tbl_Gehälter.Rows.Add("12", "1", 2500, new DateTime(2017, 2, 1));

            for (int m = 1; m < 12; m++)
            {
                tbl_Gehälter.Rows.Add(Convert.ToString(m), "2", 2500, new DateTime(2017, m, 1));
            }
            tbl_Gehälter.Rows.Add("12", "2", 2500, new DateTime(2017, 2, 1));

            DataGridMitarbeiter.ItemsSource = tbl_Mitarbeiter.DefaultView;
            DataGridGehälter.ItemsSource = tbl_Gehälter.DefaultView;

            string xmlDS = MeineDatenbank.GetXml();
            MessageBox.Show(xmlDS);

        }
    }
}
