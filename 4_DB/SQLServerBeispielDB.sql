USE [master]
GO
/****** Object:  Database [CSharpSchulung]    Script Date: 28.09.2017 16:44:29 ******/
CREATE DATABASE [CSharpSchulung]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CSharpSchulung', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\CSharpSchulung.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CSharpSchulung_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\CSharpSchulung_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CSharpSchulung] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CSharpSchulung].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CSharpSchulung] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CSharpSchulung] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CSharpSchulung] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CSharpSchulung] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CSharpSchulung] SET ARITHABORT OFF 
GO
ALTER DATABASE [CSharpSchulung] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CSharpSchulung] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CSharpSchulung] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CSharpSchulung] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CSharpSchulung] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CSharpSchulung] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CSharpSchulung] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CSharpSchulung] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CSharpSchulung] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CSharpSchulung] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CSharpSchulung] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CSharpSchulung] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CSharpSchulung] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CSharpSchulung] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CSharpSchulung] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CSharpSchulung] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CSharpSchulung] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CSharpSchulung] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CSharpSchulung] SET  MULTI_USER 
GO
ALTER DATABASE [CSharpSchulung] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CSharpSchulung] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CSharpSchulung] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CSharpSchulung] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CSharpSchulung] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CSharpSchulung]
GO
/****** Object:  Table [dbo].[tbl_DatentypBeispiel]    Script Date: 28.09.2017 16:44:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_DatentypBeispiel](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Vorname] [nvarchar](50) NULL,
	[Nachname] [nvarchar](50) NULL,
	[EinDateTime] [datetime] NULL,
	[EinDate] [date] NULL,
	[EinSmallDateTime] [smalldatetime] NULL,
	[EinFlagTinyInt] [tinyint] NULL,
	[EinFlagBit] [bit] NULL,
 CONSTRAINT [PK_tbl_DatentypBeispiel] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Gehälter]    Script Date: 28.09.2017 16:44:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Gehälter](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[MitarbeiterID] [bigint] NULL,
	[Gehalt] [real] NULL,
	[Datum] [datetime] NULL,
 CONSTRAINT [PK_tbl_Gehälter] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Mitarbeiter]    Script Date: 28.09.2017 16:44:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Mitarbeiter](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Vorname] [varchar](50) NULL,
	[Nachname] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_Mitarbeiter] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_TempData]    Script Date: 28.09.2017 16:44:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TempData](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[data] [nvarchar](max) NULL,
	[AngelegtAm] [datetime] NULL CONSTRAINT [DF_tbl_TempData_AngelegtAm]  DEFAULT (getdate()),
 CONSTRAINT [PK_tbl_TempData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_DatentypBeispiel] ON 

GO
INSERT [dbo].[tbl_DatentypBeispiel] ([id], [Vorname], [Nachname], [EinDateTime], [EinDate], [EinSmallDateTime], [EinFlagTinyInt], [EinFlagBit]) VALUES (4, N'Donald', N'Duck', CAST(N'2017-05-07 11:04:41.550' AS DateTime), CAST(N'2017-07-05' AS Date), CAST(N'2017-05-07 11:05:00' AS SmallDateTime), 1, 1)
GO
INSERT [dbo].[tbl_DatentypBeispiel] ([id], [Vorname], [Nachname], [EinDateTime], [EinDate], [EinSmallDateTime], [EinFlagTinyInt], [EinFlagBit]) VALUES (5, N'Dagobert', N'Duck', CAST(N'2017-05-07 11:04:51.347' AS DateTime), CAST(N'2017-07-05' AS Date), CAST(N'2017-05-07 11:05:00' AS SmallDateTime), 0, 0)
GO
INSERT [dbo].[tbl_DatentypBeispiel] ([id], [Vorname], [Nachname], [EinDateTime], [EinDate], [EinSmallDateTime], [EinFlagTinyInt], [EinFlagBit]) VALUES (6, NULL, N'Düsentrieb', NULL, NULL, NULL, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[tbl_DatentypBeispiel] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Gehälter] ON 

GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (1, 1, 2500, CAST(N'2017-01-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (2, 1, 2500, CAST(N'2017-02-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (3, 1, 2500, CAST(N'2017-03-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (4, 1, 2500, CAST(N'2017-04-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (5, 1, 2500, CAST(N'2017-05-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (6, 1, 2500, CAST(N'2017-06-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (7, 1, 2500, CAST(N'2017-07-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (8, 1, 2500, CAST(N'2017-08-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (9, 1, 2500, CAST(N'2017-09-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (10, 1, 2500, CAST(N'2017-10-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (11, 1, 2500, CAST(N'2017-11-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (12, 1, 5000, CAST(N'2017-12-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (13, 2, 2500, CAST(N'2017-01-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (14, 2, 2500, CAST(N'2017-02-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (15, 2, 2500, CAST(N'2017-03-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (16, 2, 2500, CAST(N'2017-04-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (17, 2, 2500, CAST(N'2017-05-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (18, 2, 2500, CAST(N'2017-06-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (19, 2, 2500, CAST(N'2017-07-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (20, 2, 2500, CAST(N'2017-08-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (21, 2, 2500, CAST(N'2017-09-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (22, 2, 2500, CAST(N'2017-10-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (23, 2, 2500, CAST(N'2017-11-01 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[tbl_Gehälter] ([id], [MitarbeiterID], [Gehalt], [Datum]) VALUES (24, 2, 5000, CAST(N'2017-12-01 00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_Gehälter] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Mitarbeiter] ON 

GO
INSERT [dbo].[tbl_Mitarbeiter] ([id], [Vorname], [Nachname]) VALUES (1, N'Donald', N'Duck')
GO
INSERT [dbo].[tbl_Mitarbeiter] ([id], [Vorname], [Nachname]) VALUES (2, N'Daniel', N'Düsentrieb')
GO
SET IDENTITY_INSERT [dbo].[tbl_Mitarbeiter] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_TempData] ON 

GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (1, N'Test', NULL)
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (2, N'Test', NULL)
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (3, N'Test', NULL)
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (4, N'Test', NULL)
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (5, N'Test', NULL)
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (6, N'dfd', CAST(N'2017-05-07 10:13:27.057' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (7, N'Test', CAST(N'2017-05-07 10:13:27.057' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (8, NULL, CAST(N'2017-05-07 11:39:42.800' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (9, NULL, CAST(N'2017-05-07 11:43:00.310' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (10, NULL, CAST(N'2017-05-07 11:43:28.097' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (11, NULL, CAST(N'2017-05-07 11:43:37.380' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (12, NULL, CAST(N'2017-09-14 12:49:06.540' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (13, NULL, CAST(N'2017-09-14 12:55:05.260' AS DateTime))
GO
INSERT [dbo].[tbl_TempData] ([id], [data], [AngelegtAm]) VALUES (14, NULL, CAST(N'2017-09-14 13:02:35.963' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_TempData] OFF
GO
USE [master]
GO
ALTER DATABASE [CSharpSchulung] SET  READ_WRITE 
GO
