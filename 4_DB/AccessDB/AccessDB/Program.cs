﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessDB
{
    class Program
    {

        static void Schreibe()
        {
            String connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\meggl\source\repos\AccessDB\CSharpSchulung.accdb;Persist Security Info=False;";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                try
                {
                    conn.Open();
                    for (int i = 0; i <= 10000; i++)
                    {
                        String SQL = String.Format("Insert into [tbl_Mitarbeiter] (Vorname,Nachname) values('Donald{0}','{1}')", i, i);

                        using (OleDbCommand cmd = new OleDbCommand(SQL, conn))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }

                    conn.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Das war nix: " + ex.Message);
                    //throw Exception(ex.Message, ex); ;
                }
            }
        }
        static void Main(string[] args)
        {
            Schreibe();
        }
    }
}
