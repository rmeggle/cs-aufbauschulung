﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.IO;

namespace ExcelReaderWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            String AktuellesVerzeichnis = Directory.GetCurrentDirectory();

            String TemplateExcelFile = AktuellesVerzeichnis + @"\..\..\..\Mappe1_template.xlsx";
            String TargetExcelFile = AktuellesVerzeichnis + @"\..\..\..\Mappe1.xlsx";

            File.Copy(TemplateExcelFile, TargetExcelFile, true);

            // msdn: https://msdn.microsoft.com/en-us/library/system.data.oledb.oledbconnectionstringbuilder(v=vs.110).aspx
            OleDbConnectionStringBuilder builder = new OleDbConnectionStringBuilder();
            builder["Provider"] = "Microsoft.ACE.OLEDB.12.0";
            builder["Data Source"] = AktuellesVerzeichnis + @"\..\..\..\Mappe1.xlsx";
            builder["Extended Properties"] = @"Excel 12.0 Xml; HDR = YES";

            //String connString = @"Provider =Microsoft.ACE.OLEDB.12.0;Data Source=" + AktuellesVerzeichnis + @"\..\..\..\Mappe1.xlsx;Extended Properties=""Excel 12.0 Xml; HDR = YES"";";
            String connString = builder.ConnectionString;

            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                try
                {
                    conn.Open();
                    for (int i = 0; i<=10000; i++)
                    {
                        String SQL = String.Empty;

                        // ISAM unterstützt leider dieses Vorgehen nicht: 
                        //
                        //String SQL_DELETE = String.Format("DELETE FROM [Tabelle1$]");
                        //using (OleDbCommand cmd = new OleDbCommand(SQL_DELETE, conn))
                        //{
                        //    cmd.ExecuteNonQuery();
                        //}

                        Console.Write(".");
                        //SQL = String.Format("INSERT INTO [Tabelle1$] (id,name) VALUES('Donald{0}','{1}')", i, i);
                        String SQL_INSERT = String.Format("INSERT INTO [Tabelle1$] (id,name) VALUES('Donald{0}','{1}')", i, i);

                        using (OleDbCommand cmd = new OleDbCommand(SQL_INSERT, conn))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }

                    conn.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Das war nix: " + ex.Message);
                    //throw Exception(ex.Message, ex); ;
                }
            }
            Console.WriteLine("Fertig");
            Console.ReadLine();
        }
    }
}
