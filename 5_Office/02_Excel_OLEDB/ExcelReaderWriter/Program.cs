﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace ExcelReaderWriter
{
    class Program
    {
        static void Schreibe()
        {
            String connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\meggl\Documents\Visual Studio 2017\Projects\cs-aufbauschulung\5_Office\02_Excel_OLEDB\Mappe1.xlsx;Extended Properties=""Excel 12.0 Xml; HDR = YES"";";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                try
                {
                    conn.Open();
                    for (int i = 0; i <= 10000; i++)
                    {
                        String SQL = String.Format("Insert into [Tabelle1$] (id,name) values('Donald{0}','{1}')", i, i);

                        using (OleDbCommand cmd = new OleDbCommand(SQL, conn))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }

                    conn.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Das war nix: " + ex.Message);
                    //throw Exception(ex.Message, ex); ;
                }
            }
        }
        static void Lese()
        {
            String connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\meggl\Documents\Visual Studio 2017\Projects\cs-aufbauschulung\5_Office\02_Excel_OLEDB\Mappe1.xlsx;Extended Properties=""Excel 12.0 Xml; HDR = YES"";";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                using (OleDbCommand cmd=new OleDbCommand("SELECT * FROM [Tabelle1$] WHERE [Name]='10' OR [Name]='9999'", conn))
                {
                    OleDbDataReader reader = cmd.ExecuteReader();
                    
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            String Ergebnis = Convert.ToString(reader[0]);
                        }
                    }
                }
                conn.Close();
            }
        }

        static void Main(string[] args)
        {
            //Schreibe();
            Lese();

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }
    }
}
