﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Person
    {
        public String Vorname{ get; set; }
        public String Nachname { get; set; }
        public int Alter { get; set; }
        public int MitarbeiterID { get; set; }
    }
}
