﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> Personenliste = new List<Person>();
            Personenliste.Add(new Person() { Vorname = "Donald", Nachname = "Duck", Alter = 23, MitarbeiterID=1 });
            Personenliste.Add(new Person() { Vorname = "Donald", Nachname = "Duck", Alter=23, MitarbeiterID = 2 });
            Personenliste.Add(new Person() { Vorname = "Dasy", Nachname = "Duck", Alter = 18, MitarbeiterID = 3 });
            Personenliste.Add(new Person() { Vorname = "Dagobert", Nachname = "Duck", Alter = 85, MitarbeiterID = 4 });

            List<Person> rentner = (from person in Personenliste
                          where person.Alter > 62
                          orderby person.Nachname descending
                          select person).ToList();

            foreach (var r in rentner)
            {
                Console.WriteLine(r.Vorname);
            }

            // Lösung mittles Lambda-Ausdruck:
            List<Person> junge = Personenliste.Where(p => p.Alter <= 23).ToList();
            

            var arbeitsfähige = from person in Personenliste
                                 where person.Alter >= 18 && person.Alter < 55
                                 orderby person.Vorname
                                 select person;

            foreach (var h in arbeitsfähige)
            {
                Console.WriteLine(h.Vorname);
            }

            Console.ReadLine();
        }
    }
}
