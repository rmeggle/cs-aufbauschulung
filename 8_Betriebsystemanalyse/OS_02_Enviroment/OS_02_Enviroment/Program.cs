﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS_02_Enviroment
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();

            Console.WriteLine("Aktuelle Applikation:        {0}", Environment.CommandLine.ToString());
            Console.WriteLine("Aktuelles Verzeichnis:       {0}", Environment.CurrentDirectory);
            Console.WriteLine("Rechnername:                 {0}", Environment.MachineName);
            Console.WriteLine("OSVersion:                   {0}", Environment.OSVersion.ToString());
            Console.WriteLine("Name des Anwenders:          {0}", Environment.UserName.ToString());
            Console.WriteLine("StackTrace:                  {0}", Environment.StackTrace);
            Console.WriteLine("SystemDirectory:             {0}", Environment.SystemDirectory);
            Console.WriteLine("Zeit in ms seit Start OS:    {0}", Environment.TickCount);
            Console.WriteLine("UserDomainName:              {0}", Environment.UserDomainName);
            Console.WriteLine("UserInteractive:             {0}", Environment.UserInteractive);
            Console.WriteLine("Physical Memory für Prozess: {0}", Environment.WorkingSet);
            Console.WriteLine("TEMP-Verzeichnis:            {0}", Environment.GetEnvironmentVariable("TEMP"));
            Console.WriteLine("APPDATA-Verzeichnis:         {0}", Environment.GetEnvironmentVariable("APPDATA"));
            Console.WriteLine("SystemRoot-Verzeichnis:      {0}", Environment.GetEnvironmentVariable("SystemRoot"));
            Console.WriteLine("Anwendungsverzeichnisse:     {0}", Environment.GetEnvironmentVariable("Path"));
            Console.WriteLine("GetFolderPath:               {0}", Environment.GetFolderPath(Environment.SpecialFolder.System));

            String[] drives = Environment.GetLogicalDrives();
            Console.WriteLine("Logische Laufwerke:          {0}", String.Join(", ", drives));



            Console.ReadLine();
        }
    }
}
