﻿using System;
using System.Collections.Generic;//  
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;

/// <summary>
/// Erforderliche Referenz: System.Management (ggf. Hinzufügen)
/// </summary>
namespace OS_01_WMI_Informationen
{
    class Program
    {
        static void Main(string[] args)
        {

            string OS_Name;
            string Version;
            string Hersteller;
            string Computer_Name;
            string Windows_Directory;
            string BootPath;

            ManagementObjectSearcher OS = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem");
            ManagementObjectCollection queryCollection1 = OS.Get();
            foreach (ManagementObject mo in queryCollection1)
            {
                OS_Name = mo["name"].ToString();
                Version = mo["version"].ToString();
                Hersteller = mo["Manufacturer"].ToString();
                Computer_Name = mo["csname"].ToString();
                Windows_Directory = mo["WindowsDirectory"].ToString();
                BootPath = mo["BootDevice"].ToString();
            }

            SelectQuery ENVIROMENTquery = new SelectQuery("Win32_Environment", "UserName=\"<SYSTEM>\"");

            ManagementObjectSearcher ENVIRIMENTsearcher = new ManagementObjectSearcher(ENVIROMENTquery);

            // Ausgabe aller Umgebungsvariablen: 
            foreach (ManagementBaseObject envVar in ENVIRIMENTsearcher.Get())
            {
                Console.WriteLine("System environment variable {0} = {1}", envVar["Name"], envVar["VariableValue"]);
            }

            // Detailinfirmationen aller laufenden Prozesse: 
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Process");
            ManagementObjectCollection collection = searcher.Get();

            foreach (ManagementObject obj in collection)
            {
                Console.Write(obj["Caption"]+";"); // Namen der Prozesse
                //Console.Write(obj["CommandLine"]);
                //Console.Write(Convert.ToString(obj["CommandLine"]));
                //Console.Write(Convert.ToString(obj["CreationClassName"]));
                //Console.Write((DateTime?)obj["CreationDate"]);
                //Console.Write(Convert.ToString(obj["CSCreationClassName"]));
                //Console.Write(Convert.ToString(obj["CSName"]));
                //Console.Write(Convert.ToString(obj["Description"]));
                //Console.Write(Convert.ToString(obj["ExecutablePath"]));

                // Weitere Variablen (mit Typangabe): 
                //(ushort?)obj["ExecutionState"] 
                //Convert.ToString(obj["Handle"]) 
                //(uint?)obj["HandleCount"] 
                //(DateTime?)obj["InstallDate"] 
                //(ulong?)obj["KernelModeTime"] 
                //(uint?)obj["MaximumWorkingSetSize"] 
                //(uint?)obj["MinimumWorkingSetSize"] 
                //Convert.ToString(obj["Name"] 
                //Convert.ToString(obj["OSCreationClassName"] 
                //Convert.ToString(obj["OSName"] 
                //(ulong?)obj["OtherOperationCount"] 
                //(ulong?)obj["OtherTransferCount"] 
                //(uint?)obj["PageFaults"] 
                //(uint?)obj["PageFileUsage"] 
                //(uint?)obj["ParentProcessId"] 
                //(uint?)obj["PeakPageFileUsage"] 
                //(ulong?)obj["PeakVirtualSize"] 
                //(uint?)obj["PeakWorkingSetSize"] 
                //(uint?)obj["Priority"] 
                //(ulong?)obj["PrivatePageCount"] 
                //(uint?)obj["ProcessId"] 
                //(uint?)obj["QuotaNonPagedPoolUsage"] 
                //(uint?)obj["QuotaPagedPoolUsage"] 
                //(uint?)obj["QuotaPeakNonPagedPoolUsage"] 
                //(uint?)obj["QuotaPeakPagedPoolUsage"] 
                //(ulong?)obj["ReadOperationCount"] 
                //(ulong?)obj["ReadTransferCount"] 
                //(uint?)obj["SessionId"] 
                //Convert.ToString(obj["Status"] 
                //(DateTime?)obj["TerminationDate"] 
                //(uint?)obj["ThreadCount"] 
                //(ulong?)obj["UserModeTime"] 
                //(ulong?)obj["VirtualSize"] 
                //Convert.ToString(obj["WindowsVersion"] 
                //(ulong?)obj["WorkingSetSize"] 
                //(ulong?)obj["WriteOperationCount"] 
                //(ulong?)obj["WriteTransferCount"] 
            }

            // Wenn man nicht alle Prozesse haben will, so kann mit der WMI-Query dies 
            // auch vorgefiltert werden: 
            // Select * From Win32_Process Where Where ParentProcessId <> 884


            // Weitere WMI-Queries: 

            // Windows Services
            // Select * From Win32_Service              

            // Event-Queries: 
            // Select * From __InstanceCreationEvent Within 5 Where TargetInstance Isa "Win32_Process"

            // Schema Queries (WMI selbst und seiner Struktur)
            // Select * From Meta_Class 

            // Dokumentation und Quelle: https://www.codeproject.com/Articles/46390/WMI-Query-Language-by-Example

            Console.ReadLine();

        }
    }
}
