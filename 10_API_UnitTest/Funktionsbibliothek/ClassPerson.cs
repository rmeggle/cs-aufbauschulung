﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funktionsbibliothek
{
    public class ClassPerson
    {

        public String Vorname { get; set; }
        public String Nachname { get; set; }

        public int Alter = 0;


        public ClassPerson()
        {

        }

        /// <summary>
        /// Ausgabe des Vor- und Nachnamens
        /// </summary>
        /// <returns></returns>
        public String VorUndNachname()
        {
            return this.Nachname + ", " + this.Vorname;
        }
    }

}
