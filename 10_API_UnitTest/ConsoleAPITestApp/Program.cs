﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Funktionsbibliothek;

namespace ConsoleAPITestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassPerson person = new ClassPerson() { Vorname="Donald", Nachname="Duck" };
            Console.WriteLine(person.VorUndNachname());
            Console.ReadLine();

        }
    }
}
