﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Funktionsbibliothek;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Ausführen des Unit-Tests über 
            // Menü -> Test -> Fenster -> Test-Explorer (oder Strg+E T)

            // Arrange
            String Vorname = "Donald";
            String Nachname = "Duck";
            String ErwarteterRückgabewert = "Duck, Donald";

            // Act
            ClassPerson Person = new ClassPerson();
            Person.Vorname = Vorname;
            Person.Nachname = Nachname;

            String TatsächlicherRückgabewert = Person.VorUndNachname();

            // Assert 
            Assert.AreEqual(ErwarteterRückgabewert, TatsächlicherRückgabewert);
            
        }
    }
}
