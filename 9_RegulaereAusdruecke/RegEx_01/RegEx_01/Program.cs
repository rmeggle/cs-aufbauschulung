﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

/// <summary>
/// Einschränkung: .NET regex kann nicht alle Regular-Expression-Ausdrücke!
/// Quick-Referenz: https://docs.microsoft.com/en-us/dotnet/standard/base-types/regular-expression-language-quick-reference
/// 
/// Mit Regular-Expressions kann man im Wesentlichen: 
/// a) Suchen
/// b) Ersetzen
/// c) Prüfen auf Existenz von (Teil)Wörtern/Bereichen
/// d) ... und sicher noch viel mehr....
/// </summary>
namespace RegEx_01
{
    class Program
    {
        static void Main(string[] args)
        {
            // i[tp] = Zeige mir alle Vorkommen von "i", welche als nächste Zeichenfolge ein "t" oder "p" haben
            RegCheck("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "i[tp]");

            // [1-8] = Zeige mir alle Vorkommen von Zahlen von 1 bis 8: 
            RegCheck("H15J9HHS0", "[1-8]");
            
            // sid wird zu ist
            RegReplace("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "sit", "ist");
            
            // Alle Leerzeichen (\s) mit einem Underscore ersetzten:
            RegReplace("Lorem ipsum dolor sit amet, consectetur adipiscing elit.", @"\s", "_");
            
            // Alle Tabulatoren mit einem Leerzeichen ersetzen
            // Vorsicht: Das Visual-Studio ersetzt TAB mit SPACE - daher wurde der Steuercode \u0009 für dieses Beispiel als TAB eingesetzt
            RegReplace("Lorem\u0009ipsum\u0009dolor\u0009sit\u0009amet,\u0009consectetur\u0009adipiscing\u0009elit.", @"\t", " "); // Geht aber nicht - sollte aber

            // Beispiel Telefonnummern: 
            // Möglich sind: 
            // 07543 608248
            // 07543 - 608248
            // + 49(0)7543 - 608248
            // + 497543 - 608248
            // 
            // Und der Ausdruck zum Prüfen lautet: 
            // ^[0 +][0 - 9] * ((\()?[0 - 9](\)))?(-|..)?[0 - 9] *

            // Grundlagen:
            // [abc]->Nur a oder b oder c erlaubt ist
            // [a - zA - Z]
            // [a]{ 1} -> Anzahl von(in dem Fall) der a´s
            //  ()->Unterabfrage, die erfüllt sein muss
            // ()? -> Unterabfrage, die erfüllt sein kann(?)
            // * ->Asterikx->alles was danach kommt
            // | ->logische oder
            // ^ ->Beginn der Zeichenkette
            // $ -> Für das Ende der Zeichenkette


            Console.ReadKey();
        }

        private static void RegCheck(String text, String expression)
        {
            Console.WriteLine("Der Ausdruck: " + expression);
            MatchCollection mc = Regex.Matches(text, expression);
            foreach (Match m in mc)
            {
                Console.WriteLine($"{m} ist korrekt");
            }
        }

        private static void RegReplace(String text, String expression, String replacement)
        {
            Regex rgx = new Regex(expression);
            string result = rgx.Replace(text, replacement);
            Console.WriteLine(result);
        }
        
    }
}
