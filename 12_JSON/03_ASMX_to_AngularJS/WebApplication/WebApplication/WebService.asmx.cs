﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace WebApplication
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetMitarbeiter()
        {
            List<ClassPerson> Personenliste = new List<ClassPerson>();
            #region "Simulierte Abfrage füllt Liste"
            Personenliste.Add(new ClassPerson() { Vorname = "Donald", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Dasy", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Daniel", Nachname = "Düsentrieb" });
            #endregion

            #region "Serialisieren der Daten"
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            String jsonData = serializer.Serialize(Personenliste);

            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.Write(jsonData);

            #endregion
        }


    }
}
