﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization; // benötigte Referenz: System.Web.Extention

namespace TCP_03_WebRequest_Basic
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "WebClient"
            // Source: https://msdn.microsoft.com/en-us/library/system.net.webclient.aspx

            WebClient client = new WebClient();

            // Bekanntgeben an dem Websserver, wer wir sind:
            client.Headers["User-Agent"] = "Mozilla/4.0 (Compatible; Windows NT 5.1; MSIE 6.0) " +
                                            "(compatible; MSIE 6.0; Windows NT 5.1; " +
                                            ".NET CLR 1.1.4322; .NET CLR 2.0.50727)";

            // Daten empfangen und in ein Byte-Array ablegen: 
            byte[] arr = client.DownloadData("http://www.merotech.de/test/data.json");

            // Prüfen ob es (z.B.) einen Content-Encoding Eintrag im Header gibt (z.B. bei html-Dateien)
            string contentEncoding = client.ResponseHeaders["Content-Encoding"]; // Etwa leer? Macht das was? 

            // oder, statt einem ByteArray gleich alle Daten holen: 
            string value = client.DownloadString("http://www.merotech.de/test/data.json");

            #endregion

            #region "WebRequest"
            // Erstellen einer Web-Request auf eine per URL erreichbaren Datei
            WebRequest request = WebRequest.Create("http://www.merotech.de/test/data.json");

            // Falls der Server will, können wir auch Standardeinstellungen verwenden
            request.Credentials = CredentialCache.DefaultCredentials;


            // Anfordern... 
            WebResponse response = request.GetResponse();

            // Lesen mit dem .NET stream-reader (viel angenehmer wie ein bytearray...)
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();

            // Ausgabe des Ergebnisses: 
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            Console.WriteLine(responseFromServer);
            // Clean up the streams and the response.
            reader.Close();
            response.Close();

            #endregion

            #region "HttpWebRequest"

            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create("http://www.merotech.de/test/data.json");
            httprequest.Credentials = CredentialCache.DefaultCredentials;

            // Oder aber - und das geht bei WebRequest nicht: 
            //httprequest.Accept = "";
            //httprequest.AllowAutoRedirect = true;
            //httprequest.MaximumAutomaticRedirections = 2;
            //httprequest.MaximumResponseHeadersLength = 4;
            //httprequest.ContentType = "text/json";
            //httprequest.KeepAlive = false;

            HttpWebResponse httpresponse = (HttpWebResponse)httprequest.GetResponse();

            Stream receiveStream = httpresponse.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream);

            String AntwortVomServer = readStream.ReadToEnd();
            Console.WriteLine(AntwortVomServer);
            #endregion


            #region "Bonustrack: Interpretation von json-Daten"

            var serializer = new JavaScriptSerializer();
            var result = serializer.DeserializeObject(AntwortVomServer);
            Console.ReadLine();

            #endregion

        }
    }
}
