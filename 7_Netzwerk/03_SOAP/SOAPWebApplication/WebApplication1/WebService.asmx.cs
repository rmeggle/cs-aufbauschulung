﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication1
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public double Addieren(double Zahl1, double Zahl2)
        {
            return Zahl1 + Zahl2;
        }

        [WebMethod]
        public double Division(double Zahl1, double Zahl2)
        {
            try
            {
                return Zahl1 / Zahl2;
            }
            catch (Exception ex)
            {

                return 0;
            }
            
        }
    }
}
