﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppSOAPClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceReferenceSOAPServer.WebServiceSoapClient server = new ServiceReferenceSOAPServer.WebServiceSoapClient();
            Console.WriteLine(server.HelloWorld());
            Console.WriteLine("Das Ergebnis von 12 und 8 bei einer Addition ist {0}", server.Addieren(12, 8));
            Console.WriteLine("Das Ergebnis der Division von 12 und 0 ist {0}",server.Division(12, 0));
            Console.ReadLine();

        }
    }
}
